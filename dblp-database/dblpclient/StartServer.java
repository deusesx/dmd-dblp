package dblpclient;

import dblpclient.remote.LightExecuter;
import dblpclient.remote.Server;

import java.io.*;

public class StartServer {
    public static void main(String[] args) throws IOException {

        LightExecuter executor = new LightExecuter();
        while (true) {
            String query = "";
            String answer = "";
            Boolean isOk = false;

            try (Server server = new Server()) { //initialize server
                query = server.recv();//receive query
                answer = executor.executeSQL(query);//"execute" query and get string as a result
                System.out.println(answer);
                isOk = server.send(answer);//send query result to client
                System.out.println(isOk);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
