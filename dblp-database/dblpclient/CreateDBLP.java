package dblpclient;

import corridadb.remote.SimpleDriver;
import corridadb.utils.Prop;

import java.io.*;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

public class CreateDBLP {
    public static void main(String[] args) {
        Connection conn = null;
        try {
            Driver d = new SimpleDriver();
            conn = d.connect(Prop.getProperty("connection"), null);
            Statement stmt = conn.createStatement();
            String[][] statements = prepareSQL();
            executeStatements(stmt, statements);
            insertData(stmt);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Create tables and Indexes
     *
     * @param stmt
     * @param statements
     * @throws SQLException
     */
    private static void executeStatements(Statement stmt, String[][] statements) throws SQLException {  //ToDo reparir to properties

        // Create tables
        for (String s : statements[0]) {
            stmt.executeUpdate(s);
            System.out.println("Table: " + s);
        }

        // Create indexes
        for (String s : statements[1]) {
            stmt.executeUpdate(s);
            System.out.println("Index: " + s);
        }

    }

    /**
     * TABLE_ID = 0, INDEX_ID= 1;
     *
     * @return statements
     */
    private static String[][] prepareSQL() {
        int INDEX_ID = 1, TABLE_ID = 0;
        String[][] statements = new String[2][];
        statements[0] = new String[5];
        statements[1] = new String[6];


        String[] tableSQLs = {"table.entity", "table.author", "table.entity_author", "table.article", "table.journal"};
        for (int i = 0; i < tableSQLs.length; i++) {
            statements[0][i] = Prop.getProperty(tableSQLs[i]);
        }

        String[] indexSQLs = {"index.entity", "index.author", "index.entity_author_e", "index.entity_author_a", "index.article", "index.journal"};
        for (int i = 0; i < indexSQLs.length; i++) {
            statements[1][i] = Prop.getProperty(indexSQLs[i]);
        }

        return statements;
    }

    private static void insertData(Statement stmt) throws SQLException {
        String[] insertSQLs = {"insert.entity", "insert.author", "insert.entity_author", "insert.article", "insert.journal"};
        String[] valFiles = {"entity_data.csv", "author_data.csv", "entity_author_data.csv", "article_data.csv", "journal_data.csv"};

        for (int i = 0; i < insertSQLs.length;i++){
            String insertSQL = Prop.getProperty(insertSQLs[i]);
            List<String> vals = getData(valFiles[i]);
            for (String val : vals) {
                System.out.println(insertSQL + val);
                stmt.executeUpdate(insertSQL + val);
            }
            System.out.println("-------------------------------------------");
        }

        System.out.println("Entity records inserted.");

    }

    private static List<String> getData(String fileName) {
        List<String> data = new LinkedList<>();
        try (BufferedReader bf = new BufferedReader(new FileReader("insert_data/" + fileName))) {
            String row = null;
            while ((row = bf.readLine()) != null) {
                data.add(" (" + row + ")");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }

}
