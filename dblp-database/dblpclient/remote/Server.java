package dblpclient.remote;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server implements AutoCloseable{

    private ServerSocket server;
    private BufferedReader in;
    private PrintWriter out;
    private Socket client;

    public Server() throws IOException {
        this.server = new ServerSocket(8080);
        this.client = server.accept();
        this.in = new BufferedReader(new InputStreamReader(client.getInputStream()));
        this.out = new PrintWriter(client.getOutputStream(),true);
    }

    public boolean send(String str) throws IOException{
        /*
            1) split @str to "packages", send number of "packages"
            2) in loop send our "packages"
         */

        int n = (int)(str.length()/1024) + 1;
        out.println(n);
        String buff = new String();

        for (int i = 0; i < n; i++){
            if (i == n-1){
                buff = str.substring(i * 1024);
            }
            else {
                buff = str.substring(i * 1024, (i + 1) * 1024);
            }
            out.println(buff);
        }

        return true;
    }

    public String recv() throws IOException{
        /*
            1) receive number of packages
            2) in loop receive all "packages" and combine in one string
            3) return combined string
         */
        String answer = new String();
        String buff = new String();

        buff = this.in.readLine();
        Integer n = Integer.parseInt(buff);

        buff = "";
        for (int i = 0; i < n; i++){
            buff = this.in.readLine();
            answer += buff.replace("\n", "");
        }
        return answer;
    }

    public void close() throws Exception{
        if (server != null){
            server.close();
        }
        if (client != null){
            client.close();
        }
        if (in != null){
            in.close();
        }
        if(out != null){
            out.close();
        }

    }
}

