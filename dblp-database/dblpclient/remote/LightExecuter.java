package dblpclient.remote;

import corridadb.remote.SimpleDriver;
import corridadb.utils.Prop;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

/**
 * use cases:
 * update entity set title='test' where entity-id=1
 * insert into entity(entity-id, mdate, title, type) values (1, '2014-02-05', 'test record', 'article')
 * delete from entity where entity-id=1
 */
public class LightExecuter {


    private Map<String, Boolean> getTypeMap;

    public LightExecuter() {
        getTypeMap = new HashMap<>();
        getTypeMap.put("entity-id", true);
        getTypeMap.put("mdate", false);
        getTypeMap.put("title", false);
        getTypeMap.put("type", false);
        getTypeMap.put("author-id", true);
        getTypeMap.put("author-name", false);
        getTypeMap.put("eid", true);
        getTypeMap.put("aid", true);
        getTypeMap.put("article-id", true);
        getTypeMap.put("pages", false);
        getTypeMap.put("volume", true);
        getTypeMap.put("year", true);
        getTypeMap.put("journal-id", true);
        getTypeMap.put("num", false);
        getTypeMap.put("journal-id", true);
        getTypeMap.put("journal-name", false);

    }

    private String[] getColumns(String qry) {
        String[] lexems = qry.split(",\\s|\\s");
        int lastLexIndx = 0;
        String[] columns = null;
        if (lexems[0].equals("select")) {
            for (; lastLexIndx < lexems.length && !lexems[lastLexIndx].equals("from"); lastLexIndx++) ;

            columns = new String[lastLexIndx - 1];
            System.arraycopy(lexems, 1, columns, 0, lastLexIndx - 1);
        }
        System.out.print("Columns: ");
        for (String c : columns) {
            System.out.print(c + ", ");
        }
        System.out.println();
        return columns;
    }


    /**
     * Execuutes statements ans returns in jquery style
     *
     * @param qry
     * @return
     */
    public String executeSQL(String qry) {
        qry = qry.trim().toLowerCase();
        String response = "";

        Connection conn = null;
        try {
            // Step 1: connect to database server
            Driver d = new SimpleDriver();
            conn = d.connect(Prop.getProperty("connection"), null);
            // Step 2: execute the query
            Statement stmt = conn.createStatement();

            if (qry.startsWith("search:")) {
                response = executeSearchTitle(qry, stmt);
            } else if (qry.startsWith("select")) {
                response = executeSelect(qry, stmt);
            } else if (qry.startsWith("update")) {
                response = executeUpdate(qry, stmt);
            } else if (qry.startsWith("insert")) {
                response = executeInsert(qry, stmt);
            } else if (qry.startsWith("delete")) {
                response = executeDelete(qry, stmt);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // Step 4: close the connection
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return response;
    }

    private String executeSearchTitle(String qry, Statement stmt) throws SQLException {
        String response = "";

        String toFind = qry.substring(7);
        System.out.println("Search: |" + toFind + "|");
        ResultSet rs = stmt.executeQuery("select entity-id, mdate, title, type from entity");

        // Step 3: loop through the result set
        StringBuilder sb = new StringBuilder();
        sb.append("[");

        boolean isFirstItem = true;

        while (rs.next()) {
            String res = rs.getString("title").toLowerCase();
            if (res.contains(toFind)) {
                if (!isFirstItem) {
                    sb.append(",");
                }
                sb.append("{\"entity-id\":");
                sb.append(rs.getInt("entity-id"));
                sb.append(",\"mdate\":\"");
                sb.append(rs.getString("mdate"));
                sb.append("\",\"title\":\"");
                sb.append(rs.getString("title"));
                sb.append("\",\"type\":\"");
                sb.append(rs.getString("type"));
                sb.append("\"}");
                isFirstItem = false;
            }
        }

        sb.append("]");
        response = sb.toString();
        rs.close();


        return response;
    }

    private String executeSelect(String qry, Statement stmt) throws SQLException {
        String response = "";
        String[] columns = getColumns(qry);
        ResultSet rs = stmt.executeQuery(qry);

        // Step 3: loop through the result set
        StringBuilder sb = new StringBuilder();
        sb.append("[");

        boolean isFirstItem = true;

        while (rs.next()) {
            if (!isFirstItem) {
                sb.append(",");
            }
            sb.append("{");
            for (int i = 0; i < columns.length; i++) {
                if (i > 0) {
                    sb.append(",");
                }
                sb.append("\"");
                sb.append(columns[i]);
                sb.append("\":\"");
                if (getTypeMap.get(columns[i])) {
                    sb.append(rs.getInt(columns[i]));
                } else {
                    sb.append(rs.getString(columns[i]));
                }
                sb.append("\"");
            }
            sb.append("}");
            isFirstItem = false;
        }
        sb.append("]");
        response = sb.toString();
        rs.close();
        return response;
    }

    private String executeUpdate(String qry, Statement stmt) throws SQLException {
        System.out.println(qry);
        stmt.executeUpdate(qry);
        return "{\"status\":\"OK\"}";

    }

    private String executeInsert(String qry, Statement stmt) throws SQLException {
        System.out.println(qry);
        stmt.execute(qry);
        return "{\"status\":\"OK\"}";

    }

    private String executeDelete(String qry, Statement stmt) throws SQLException {
        System.out.println(qry);
        stmt.executeUpdate(qry);
        return "{\"status\":\"OK\"}";
    }
}
