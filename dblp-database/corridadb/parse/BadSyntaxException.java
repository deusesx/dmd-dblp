package corridadb.parse;

/**
 * A runtime exception indicating that the submitted query
 * has incorrect syntax.
 */
@SuppressWarnings("serial")
public class BadSyntaxException extends RuntimeException {
   public BadSyntaxException() {
   }
}
