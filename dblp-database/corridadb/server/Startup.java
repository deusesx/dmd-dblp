package corridadb.server;

import corridadb.remote.*;
import corridadb.utils.Prop;

import java.rmi.registry.*;

public class Startup {
   public static void main(String args[]) throws Exception {
      // configure and initialize the database
      SimpleDB.init(Prop.getProperty("database_name"));
      
      // create a registry specific for the server on the default port
      Registry reg = LocateRegistry.createRegistry(1099);
      
      // and post the server entry in it
      RemoteDriver d = new RemoteDriverImpl();
      reg.rebind("corridadb", d);
      
      System.out.println("database server ready");
   }
}
