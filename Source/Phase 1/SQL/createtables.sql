CREATE TABLE dblp_author
(
    author_id INT PRIMARY KEY NOT NULL,
    author VARCHAR
);
CREATE TABLE dblp_editor
(
    editor_id INT PRIMARY KEY NOT NULL,
    editor VARCHAR
);
CREATE TABLE dblp_entity
(
    entity_id INT PRIMARY KEY NOT NULL,
    key VARCHAR,
    mdate DATE,
    title VARCHAR
);
CREATE TABLE dblp_entity_article
(
    entity_id INT PRIMARY KEY NOT NULL,
    pages INT4RANGE,
    volume INT,
    year INT,
    journal_id INT,
    num INT
);
CREATE TABLE dblp_entity_author
(
    entity_id INT NOT NULL,
    author_id INT NOT NULL,
    PRIMARY KEY (entity_id, author_id)
);
CREATE TABLE dblp_entity_book
(
    entity_id INT PRIMARY KEY NOT NULL,
    booktitle VARCHAR,
    volume INT,
    year INT
);
CREATE TABLE dblp_entity_cdrom
(
    entity_id INT NOT NULL,
    cdrom VARCHAR NOT NULL,
    PRIMARY KEY (entity_id, cdrom)
);
CREATE TABLE dblp_entity_citation
(
    entity_id INT PRIMARY KEY NOT NULL,
    citedentity_id INT
);
CREATE TABLE dblp_entity_editor
(
    editor_id INT NOT NULL,
    entity_id INT NOT NULL,
    PRIMARY KEY (editor_id, entity_id)
);
CREATE TABLE dblp_entity_ee
(
    entity_id INT NOT NULL,
    ee VARCHAR NOT NULL,
    PRIMARY KEY (entity_id, ee)
);
CREATE TABLE dblp_entity_incollection
(
    entity_id INT PRIMARY KEY NOT NULL,
    booktitle VARCHAR,
    crossref INT,
    pages INT4RANGE,
    year INT
);
CREATE TABLE dblp_entity_inproceedings
(
    entity_id INT PRIMARY KEY NOT NULL,
    booktitle VARCHAR,
    crossref INT,
    pages INT4RANGE,
    year INT
);
CREATE TABLE dblp_entity_isbn
(
    entity_id INT NOT NULL,
    isbn VARCHAR NOT NULL,
    PRIMARY KEY (entity_id, isbn)
);
CREATE TABLE dblp_entity_msthesis
(
    entity_id INT PRIMARY KEY NOT NULL,
    school VARCHAR,
    year INT
);
CREATE TABLE dblp_entity_note
(
    entity_id INT NOT NULL,
    note VARCHAR NOT NULL,
    PRIMARY KEY (entity_id, note)
);
CREATE TABLE dblp_entity_phdthesis
(
    school VARCHAR,
    year INT,
    entity_id INT PRIMARY KEY NOT NULL
);
CREATE TABLE dblp_entity_proceedings
(
    entity_id INT PRIMARY KEY NOT NULL,
    booktitle VARCHAR,
    series VARCHAR,
    volume INT,
    year INT
);
CREATE TABLE dblp_entity_publisher
(
    publisher_id INT NOT NULL,
    entity_id INT NOT NULL,
    PRIMARY KEY (publisher_id, entity_id)
);
CREATE TABLE dblp_entity_url
(
    entity_id INT NOT NULL,
    url VARCHAR NOT NULL,
    PRIMARY KEY (entity_id, url)
);
CREATE TABLE dblp_journal
(
    journal_id INT PRIMARY KEY NOT NULL,
    name VARCHAR
);
CREATE TABLE dblp_publisher
(
    publisher_id INT PRIMARY KEY NOT NULL,
    publisher VARCHAR
);
ALTER TABLE dblp_entity_article ADD FOREIGN KEY (entity_id) REFERENCES dblp_entity (entity_id);
ALTER TABLE dblp_entity_article ADD FOREIGN KEY (journal_id) REFERENCES dblp_journal (journal_id);
ALTER TABLE dblp_entity_author ADD FOREIGN KEY (author_id) REFERENCES dblp_author (author_id);
ALTER TABLE dblp_entity_author ADD FOREIGN KEY (entity_id) REFERENCES dblp_entity (entity_id);
ALTER TABLE dblp_entity_book ADD FOREIGN KEY (entity_id) REFERENCES dblp_entity (entity_id);
ALTER TABLE dblp_entity_cdrom ADD FOREIGN KEY (entity_id) REFERENCES dblp_entity (entity_id);
ALTER TABLE dblp_entity_citation ADD FOREIGN KEY (citedentity_id) REFERENCES dblp_entity (entity_id);
ALTER TABLE dblp_entity_editor ADD FOREIGN KEY (editor_id) REFERENCES dblp_editor (editor_id);
ALTER TABLE dblp_entity_editor ADD FOREIGN KEY (entity_id) REFERENCES dblp_entity (entity_id);
ALTER TABLE dblp_entity_ee ADD FOREIGN KEY (entity_id) REFERENCES dblp_entity (entity_id);
ALTER TABLE dblp_entity_incollection ADD FOREIGN KEY (crossref) REFERENCES dblp_entity (entity_id);
ALTER TABLE dblp_entity_incollection ADD FOREIGN KEY (entity_id) REFERENCES dblp_entity (entity_id);
ALTER TABLE dblp_entity_inproceedings ADD FOREIGN KEY (crossref) REFERENCES dblp_entity (entity_id);
ALTER TABLE dblp_entity_inproceedings ADD FOREIGN KEY (entity_id) REFERENCES dblp_entity (entity_id);
CREATE INDEX fki_crossreference ON dblp_entity_inproceedings (crossref);
ALTER TABLE dblp_entity_isbn ADD FOREIGN KEY (entity_id) REFERENCES dblp_entity (entity_id);
ALTER TABLE dblp_entity_msthesis ADD FOREIGN KEY (entity_id) REFERENCES dblp_entity (entity_id);
ALTER TABLE dblp_entity_note ADD FOREIGN KEY (entity_id) REFERENCES dblp_entity (entity_id);
ALTER TABLE dblp_entity_phdthesis ADD FOREIGN KEY (entity_id) REFERENCES dblp_entity (entity_id);
ALTER TABLE dblp_entity_proceedings ADD FOREIGN KEY (entity_id) REFERENCES dblp_entity (entity_id);
ALTER TABLE dblp_entity_publisher ADD FOREIGN KEY (entity_id) REFERENCES dblp_entity (entity_id);
ALTER TABLE dblp_entity_publisher ADD FOREIGN KEY (publisher_id) REFERENCES dblp_publisher (publisher_id);
ALTER TABLE dblp_entity_url ADD FOREIGN KEY (entity_id) REFERENCES dblp_entity (entity_id);
