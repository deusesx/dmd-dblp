package dblp.persist;

import dblp.utils.Prop;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.*;

import static dblp.persist.IND.*;

public class PersistContainer {
    private static final Logger logger = LoggerFactory.getLogger(PersistContainer.class);
    final private static String DELIMITER = Prop.getProperty("dblp.parser.delimiter"),
            STORE_FOLDER = Prop.getProperty("dblp.parse.csv_folder"), FILE_EXTN = Prop.getProperty("dblp.persist.extension");
    private String[] standaloneFileNames, itemFileNames, entityFileNames, foreignKeyFileNames;
    private final int FG_ENTITY = 0, FG_ITEMS = 1, FG_FK = 2, ROW_COUNT_FLUSH = 1000;
    private static PersistContainer perCont;

    private PersistContainer() {
        try {
            checkOrCreateFolder();
        } catch (IOException e) {
            e.printStackTrace();
        }
        initEntityFileNames();
        initItemFileNames();
        initStandaloneFileNames();
        initForeignKeyFileNames();

        writeHeadersToFiles();
    }

    private void writeHeadersStandalone() {
        String[] headers = new String[COUNT_STANDALONE];
        headers[IND_AUTHOR] = Prop.getProperty("dblp.persist.columns_author");
        headers[IND_EDITOR] = Prop.getProperty("dblp.persist.columns_editor");
        headers[IND_JOURNAL] = Prop.getProperty("dblp.persist.columns_journal");
        headers[IND_PUBLISHER] = Prop.getProperty("dblp.persist.columns_publisher");
        headers[IND_SCHOOL] = Prop.getProperty("dblp.persist.columns_school");
        headers[IND_SERIES] = Prop.getProperty("dblp.persist.columns_series");

        for (int i = 0; i < COUNT_STANDALONE; i++) {
            if (headers[i] != null) {
                writeHeaderTo(standaloneFileNames[i], headers[i]);
            }
        }
    }

    private void writeHeadersItems() {
        String[] headers = new String[COUNT_ITEM];
        headers[IND_ISBN] = Prop.getProperty("dblp.persist.columns_isbn");
        headers[IND_URL] = Prop.getProperty("dblp.persist.columns_url");
        headers[IND_EE] = Prop.getProperty("dblp.persist.columns_ee");
        headers[IND_NOTE] = Prop.getProperty("dblp.persist.columns_note");

        for (int i = 0; i < COUNT_ITEM; i++) {
            if (headers[i] != null) {
                writeHeaderTo(itemFileNames[i], headers[i]);
            }
        }
    }

    private void writeHeadersEntity() {
        String[] headers = new String[COUNT_ENTITY];
        headers[IND_BOOK] = Prop.getProperty("dblp.persist.columns_book");
        headers[IND_THESIS] = Prop.getProperty("dblp.persist.columns_thesis");
        headers[IND_ARTICLE] = Prop.getProperty("dblp.persist.columns_article");
        headers[IND_RECORD] = Prop.getProperty("dblp.persist.columns_record");

        for (int i = 0; i < COUNT_ENTITY; i++) {
            if (headers[i] != null) {
                writeHeaderTo(entityFileNames[i], headers[i]);
            }
        }
    }

    private void writeHeadersFK() {
        String[] headers = new String[COUNT_FK];
        headers[IND_AUTHOR] = Prop.getProperty("dblp.persist.columns_fk_author");
        headers[IND_EDITOR] = Prop.getProperty("dblp.persist.columns_fk_editor");
        headers[IND_PUBLISHER] = Prop.getProperty("dblp.persist.columns_fk_publisher");

        for (int i = 0; i < COUNT_FK; i++) {
            if (headers[i] != null) {
                writeHeaderTo(foreignKeyFileNames[i], headers[i]);
            }
        }
    }

    private void writeHeaderTo(String fileName, String header) {
        try (BufferedWriter bw = initWriter(fileName, false)) {
            bw.write(header.replace(",", DELIMITER));
            bw.write("\n");
            bw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeHeadersToFiles() {
        writeHeadersStandalone();
        writeHeadersEntity();
        writeHeadersItems();
        writeHeadersFK();
    }

    static PersistContainer getSingleton() {
        if (perCont != null) {
            return perCont;
        } else {
            perCont = new PersistContainer();
            return perCont;
        }
    }

    private void initStandaloneFileNames() {
        standaloneFileNames = new String[COUNT_STANDALONE];
        standaloneFileNames[IND_AUTHOR] = Prop.getProperty("dblp.persist.tbname_author");
        standaloneFileNames[IND_CITE] = Prop.getProperty("dblp.persist.tbname_cite_temp");
        standaloneFileNames[IND_EDITOR] = Prop.getProperty("dblp.persist.tbname_editor");
        standaloneFileNames[IND_JOURNAL] = Prop.getProperty("dblp.persist.tbname_journal");
        standaloneFileNames[IND_PUBLISHER] = Prop.getProperty("dblp.persist.tbname_publisher");
        standaloneFileNames[IND_CROSSREF] = Prop.getProperty("dblp.persist.tbname_crossref_temp");
        standaloneFileNames[IND_SCHOOL] = Prop.getProperty("dblp.persist.tbname_school");
        standaloneFileNames[IND_SERIES] = Prop.getProperty("dblp.persist.tbname_series");
    }

    private void initItemFileNames() {
        itemFileNames = new String[COUNT_ITEM];
        itemFileNames[IND_ISBN] = Prop.getProperty("dblp.persist.tbname_isbn");
        itemFileNames[IND_URL] = Prop.getProperty("dblp.persist.tbname_url");
        itemFileNames[IND_EE] = Prop.getProperty("dblp.persist.tbname_ee");
        itemFileNames[IND_NOTE] = Prop.getProperty("dblp.persist.tbname_note");
    }

    private void initEntityFileNames() {
        entityFileNames = new String[COUNT_ENTITY];
        entityFileNames[IND_BOOK] = Prop.getProperty("dblp.persist.tbname_book");
        entityFileNames[IND_INCLUSION] = Prop.getProperty("dblp.persist.tbname_inclusion_temp");
        entityFileNames[IND_THESIS] = Prop.getProperty("dblp.persist.tbname_thesis");
        entityFileNames[IND_ARTICLE] = Prop.getProperty("dblp.persist.tbname_article");
        entityFileNames[IND_RECORD] = Prop.getProperty("dblp.persist.tbname_record");
    }

    private void initForeignKeyFileNames() {
        foreignKeyFileNames = new String[COUNT_FK];
        foreignKeyFileNames[IND_AUTHOR] = Prop.getProperty("dblp.persist.tbname_fk_author");
        foreignKeyFileNames[IND_EDITOR] = Prop.getProperty("dblp.persist.tbname_fk_editor");
        foreignKeyFileNames[IND_PUBLISHER] = Prop.getProperty("dblp.persist.tbname_fk_publisher");
        foreignKeyFileNames[IND_CITE] = Prop.getProperty("dblp.persist.tbname_fk_cite_temp");
    }

    private void checkOrCreateFolder() throws IOException {
        final File folder = new File(Prop.getProperty("dblp.parse.csv_folder"));
        if (!folder.exists()) {
            folder.mkdir();
            logger.info("folder {} is create", STORE_FOLDER);
        }
    }

    void saveAndEmptyEntities(List[] entityContainer) {
        saveData(entityContainer, FG_ENTITY);
        clearLists(entityContainer);
        logger.info("Entities are persisted");
    }

    void saveAndEmptyItems(List[] itemContainer) {
        saveData(itemContainer, FG_ITEMS);
        clearLists(itemContainer);
        logger.info("Items persisted");
    }

    void saveAndEmptyFK(List[] fkContainer) {
        saveData(fkContainer, FG_FK);
        clearLists(fkContainer);
        logger.info("Foreign keys are persisted");
    }

    private void clearLists(List[] dataContainer) {
        for (List dataList : dataContainer) {
            dataList.clear();
        }
    }

    private void saveData(List[] dataContainer, int listType) {
        String[] fileNames = (listType == FG_ENTITY) ? entityFileNames : (listType == FG_ITEMS) ? itemFileNames : foreignKeyFileNames;
        for (int i = 0; i < dataContainer.length; i++) {
            saveListDataToFile(dataContainer[i], fileNames[i]);
        }
    }

    private void saveListDataToFile(List dataList, String fileName) {
        try (BufferedWriter bw = initWriter(fileName, true)) {
            int i = 0;
            for (Object row : dataList) {
                bw.write(row.toString());
                bw.write("\n");
                if (i++ > ROW_COUNT_FLUSH) {
                    bw.flush();
                    i = 0;
                }
            }
            bw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void saveStandalones(Map[] dataMaps) {
        for (int i = 0; i < dataMaps.length; i++) {
            try (BufferedWriter bw = initWriter(standaloneFileNames[i], true)) {
                int j = 0;
                for (Map.Entry entry : ((Map<String, Integer>) dataMaps[i]).entrySet()) {
                    bw.write(entry.getValue() + DELIMITER + entry.getKey());
                    bw.write("\n");
                    if (j++ > ROW_COUNT_FLUSH) {
                        bw.flush();
                        j = 0;
                    }
                }
                bw.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        logger.info("Standalone are persisted");
    }

    private BufferedWriter initWriter(String fileNameNoExt, boolean doAppend) throws IOException {
        return new BufferedWriter(new FileWriter(new File(STORE_FOLDER, fileNameNoExt + FILE_EXTN), doAppend));
    }

}