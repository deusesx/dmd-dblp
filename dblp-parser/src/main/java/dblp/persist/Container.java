package dblp.persist;

import dblp.model.*;
import dblp.parser.entity_parser.StructForeigns;
import dblp.parser.schema.Author;
import dblp.parser.schema.*;
import dblp.parser.schema.Editor;
import dblp.parser.schema.Journal;
import dblp.parser.schema.Publisher;
import dblp.utils.Prop;

import java.util.*;

import static dblp.persist.IND.*;

public class Container {
    //<One-To-Many>
    private final int[] lastIds;
    private int lastRecordId;
    private final Map<String, Integer>[] standaloneContainer;
    private final List<StructForeigns>[] foreignKeyContainer;

    //<One-To-One>
    private final List<String>[] itemContainer;

    //record, article, book, inclusion, thesis
    private final List<String>[] entyContainer;
    final private static String DELIMITER = Prop.getProperty("dblp.parser.delimiter");
    private int rowCounter;
    private final static int ROW_COUNT_TO_FLUSH = 6000;
    private Set<String> checkRepeatCite = new HashSet<>();

    private static Container container;

    @SuppressWarnings("unchecked")
    private Container() {
        lastIds = new int[COUNT_STANDALONE + 1];
        standaloneContainer = new Map[COUNT_STANDALONE];
        initStandaloneContainer();

        foreignKeyContainer = new List[COUNT_FK];
        initForeignContainer();

        itemContainer = new List[COUNT_ITEM];
        initItemsContainer();

        entyContainer = new List[COUNT_ENTITY];
        initEntityContainer();

        rowCounter = 0;
    }

    public static Container getSingleton() {
        if (container != null) {
            return container;
        } else {
            container = new Container();
            return container;
        }
    }

    private void initForeignContainer() {
        for (int i = 0; i < foreignKeyContainer.length; i++) {
            foreignKeyContainer[i] = new LinkedList<>();
        }
    }

    private void initEntityContainer() {
        for (int i = 0; i < entyContainer.length; i++) {
            entyContainer[i] = new LinkedList<>();
        }
        lastRecordId = START_ID;
    }

    private void initItemsContainer() {
        for (int i = 0; i < itemContainer.length; i++) {
            itemContainer[i] = new LinkedList<>();
        }
    }

    private void initStandaloneContainer() {
        initAuthorContainer();
        initCiteContainer();
        initEditorContainer();
        initJournalContainer();
        initPublisherContainer();
        initCrossrefContainer();
        initSchoolContainer();
        initSeriesContainer();
    }

    private void initSeriesContainer() {
        standaloneContainer[IND_SERIES] = new LinkedHashMap<>();
        lastIds[IND_SERIES] = START_ID;
    }

    private void initSchoolContainer() {
        standaloneContainer[IND_SCHOOL] = new LinkedHashMap<>();
        lastIds[IND_SCHOOL] = START_ID;
    }

    private void initCrossrefContainer() {
        standaloneContainer[IND_CROSSREF] = new LinkedHashMap<>();
        lastIds[IND_CROSSREF] = START_ID;
    }

    private void initAuthorContainer() {
        standaloneContainer[IND_AUTHOR] = new LinkedHashMap<>();
        lastIds[IND_AUTHOR] = START_ID; //ToDo decide how to init value
    }

    private void initEditorContainer() {
        standaloneContainer[IND_EDITOR] = new LinkedHashMap<>();
        lastIds[IND_EDITOR] = START_ID;
    }

    private void initPublisherContainer() {
        standaloneContainer[IND_PUBLISHER] = new LinkedHashMap<>();
        lastIds[IND_PUBLISHER] = START_ID;
    }

    private void initJournalContainer() {
        standaloneContainer[IND_JOURNAL] = new LinkedHashMap<>();
        lastIds[IND_JOURNAL] = START_ID;
    }

    private void initCiteContainer() {
        standaloneContainer[IND_CITE] = new LinkedHashMap<>();
        lastIds[IND_CITE] = START_ID;
    }

    public Integer getSchoolId(Object obj) {
        return getFromMap(IND_SCHOOL, ((School) obj).getvalue());
    }

    public Integer getSeriesId(Object obj) {
        Series series = (Series) obj;
        return getFromMap(IND_SERIES, series.getvalue() + DELIMITER + series.getHref());
    }

    public Integer getCrossRefId(Object obj) {
        return getFromMap(IND_CROSSREF, ((Crossref) obj).getvalue());
    }

    public Integer getJournalId(Object obj) {
        return getFromMap(IND_JOURNAL, ((Journal) obj).getvalue());
    }

    private Integer getAuthorId(Object obj) {
        return getFromMap(IND_AUTHOR, ((Author) obj).getvalue()); //ToDo find about bibTex attribute
    }

    private Integer getEditorId(Object obj) {
        return getFromMap(IND_EDITOR, ((Editor) obj).getvalue());
    }

    private Integer getPublisherId(Object obj) {
        Publisher publisher = (Publisher) obj;
        return getFromMap(IND_PUBLISHER, publisher.getvalue());
    }

    private Integer getCiteId(Object obj) {
        return getFromMap(IND_CITE, ((Cite) obj).getvalue());
    }

    private int getFromMap(int containerInd, String name) {
        Map<String, Integer> temp = standaloneContainer[containerInd];
        int id = (temp.containsKey(name)) ? temp.get(name) : NOT_FOUND_ID;
        if (id == NOT_FOUND_ID) {
            id = lastIds[containerInd]++;
            temp.put(name, id);
        }
        return id;
    }

    public void addEE(Object item, int recordId) {
        itemContainer[IND_EE].add(recordId + DELIMITER + ((Ee) item).getvalue());
    }

    public void addUrl(Object item, int recordId) {
        itemContainer[IND_URL].add(recordId + DELIMITER + ((Url) item).getvalue());
    }

    public void addISBL(Object item, int recordId) {
        itemContainer[IND_ISBN].add(recordId + DELIMITER + ((Isbn) item).getvalue());
    }

    public void addNote(Object item, int recordId) {
        Note note = (Note) item;
        itemContainer[IND_NOTE].add(recordId + DELIMITER + note.getvalue() + DELIMITER + note.getType());
    }

    /**
     * Define order of attributes yourself
     */
    public void addRecord(Record record) {
        entyContainer[IND_RECORD].add(record.toString());
    }

    public void addArticle(RecordArticle article) {
        entyContainer[IND_ARTICLE].add(article.toString());
    }

    public void addBook(RecordBook book) {
        entyContainer[IND_BOOK].add(book.toString());
    }

    public void addInclusion(RecordInclusion inclusion) {
        entyContainer[IND_INCLUSION].add(inclusion.toString());
    }

    public void addThesis(RecordThesis thesis) {
        entyContainer[IND_THESIS].add(thesis.toString());
    }

    //Foreigners
    public void addAuthor(Object obj, int recId) {
        foreignKeyContainer[IND_AUTHOR].add(new StructForeigns(recId, getAuthorId(obj)));
    }

    public void addEditor(Object obj, int recId) {
        foreignKeyContainer[IND_EDITOR].add(new StructForeigns(recId, getEditorId(obj)));
    }

    public void addPublisher(Object obj, int recId) {
        foreignKeyContainer[IND_PUBLISHER].add(new StructForeigns(recId, getPublisherId(obj)));
    }

    public void addCite(Object obj, int recId) {
        Integer citeId = getCiteId(obj);
        if (!checkRepeatCite.contains(recId + DELIMITER + citeId)) {
            foreignKeyContainer[IND_CITE].add(new StructForeigns(recId, citeId));
            checkRepeatCite.add(recId + DELIMITER + citeId);
        }
    }

    public void cleanCheckCite() {
        checkRepeatCite.clear();
    }

    //Creators
    public Record createRecord() {
        if (rowCounter++ > ROW_COUNT_TO_FLUSH) {
            persistAllData();
            rowCounter = 0;
        }
        return new Record().setRecordId(lastRecordId++);
    }

    public RecordArticle createArticle(int recordId) {
        return new RecordArticle(recordId);
    }

    public RecordBook createBook(int recordId) {
        return new RecordBook(recordId);
    }

    public RecordInclusion createIncollectionOrInproceedings(int recordId) {
        return new RecordInclusion(recordId);
    }

    public RecordThesis createThesis(int recordId) {
        return new RecordThesis(recordId);
    }

    public void persistAllData() {
        PersistContainer persCont = PersistContainer.getSingleton();
        persCont.saveAndEmptyEntities(entyContainer);
        persCont.saveAndEmptyItems(itemContainer);
        persCont.saveAndEmptyFK(foreignKeyContainer);
        persCont.saveStandalones(standaloneContainer);
    }

}