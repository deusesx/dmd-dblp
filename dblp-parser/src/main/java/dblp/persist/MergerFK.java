package dblp.persist;

import dblp.utils.DataUtils;
import dblp.utils.Prop;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MergerFK {

    private Map<String, String> recordKeyIdMap;
    private static final int ROW_COUNT_FLUSH = 1000, ARRAY_SIZE = 30000;
    final private static String DELIMITER = Prop.getProperty("dblp.parser.delimiter"),
            STORE_FOLDER = Prop.getProperty("dblp.parse.csv_folder"),
            FILE_EXTN = Prop.getProperty("dblp.persist.extension");

    public void mergeAllFK() {
        recordKeyIdMap = getRecordKeyIdMap();
        mergeCiteRecord();
//        mergeCrossrefRecord();
        recordKeyIdMap.clear();
    }

    private void mergeCrossrefRecord() {
        List<String> crossrefKeyList = getStandAlone(Prop.getProperty("dblp.persist.tbname_crossref_temp"));
        List<String> inclusionList = mergeCrossRefFK(crossrefKeyList);
        saveListDataToFile(inclusionList, Prop.getProperty("dblp.persist.tbname_inclusion"), Prop.getProperty("dblp.persist.columns_inclusion"));
        inclusionList.clear();
        crossrefKeyList.clear();
    }

    private List<String> mergeCrossRefFK(List<String> crossrefId_KeyList) {
        List<String> inclusionUpdatedList = new LinkedList<>();
        String fileName = Prop.getProperty("dblp.persist.tbname_inclusion_temp");
        try (BufferedReader br = initReader(fileName)) {
            String row;
            Pattern pattern = Pattern.compile("([^,]+,[^,]+,[^,]+,[^,]+,[^,]+,)([^,]+)".replace(",", "\\"+DELIMITER));
            while ((row = br.readLine()) != null) {
                Matcher matcher = pattern.matcher(row);
                if (matcher.matches()) {
                    int crossrefId = Integer.valueOf(matcher.group(2));
                    String recordKey = crossrefId_KeyList.get(crossrefId);
                    String entityId = getRecIdFromKey(recordKey);
                    inclusionUpdatedList.add(matcher.group(1) + entityId);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return inclusionUpdatedList;
    }

    private void mergeCiteRecord() {
        List<String> citeId_KeyList = getStandAlone(Prop.getProperty("dblp.persist.tbname_cite_temp"));
        List<String> resCiteFk = mergeCiteRecordByKey(citeId_KeyList);
        saveListDataToFile(resCiteFk, Prop.getProperty("dblp.persist.tbname_cite"), Prop.getProperty("dblp.persist.columns_cite"));
        citeId_KeyList.clear();
        resCiteFk.clear();
    }

    private Map<String, String> getRecordKeyIdMap() {
        Map<String, String> recordKeyIdMap = new HashMap<>();
        String fileName = Prop.getProperty("dblp.persist.tbname_record");
        Pattern pattern = Pattern.compile("([^,]+),([^,]+),[^,]+,[^,]+,[^,]+".replace(",", "\\" + DELIMITER));
        Matcher matcher;
        try (BufferedReader br = initReader(fileName)) {
            String row;
            while ((row = br.readLine()) != null) {
                matcher = pattern.matcher(row);
                if (matcher.matches()) {
                    recordKeyIdMap.put(matcher.group(2).trim(), matcher.group(1).trim());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return recordKeyIdMap;
    }

    private List<String> getStandAlone(String fileName) {
        ArrayList<String> Id_KeyList = new ArrayList<>(ARRAY_SIZE);
        Id_KeyList.add(null); // remove index 0
        try (BufferedReader br = initReader(fileName)) {
            String row;
            while ((row = br.readLine()) != null) {
                String value = getLstPartOfPair(row);
                Id_KeyList.add(value);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Id_KeyList;
    }

    private List<String> mergeCiteRecordByKey(List<String> citeId_KeyList) {
        List<String> resCitefk = new LinkedList<>();
        String fileName = Prop.getProperty("dblp.persist.tbname_fk_cite_temp");
        try (BufferedReader br = initReader(fileName)) {
            String row, key, recId;
            while (!DataUtils.isEmpty(row = br.readLine())) {
                key = citeId_KeyList.get(Integer.valueOf(getLstPartOfPair(row))).trim();
                if (key.equals("...")) {
                    continue;
                }
                recId = getRecIdFromKey(key);
                if (!DataUtils.isEmpty(recId)) {
                    resCitefk.add(getFirstPartOfPair(row) + DELIMITER + recId);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return resCitefk;
    }

    private BufferedReader initReader(String fileName) throws FileNotFoundException {
        return new BufferedReader(new FileReader(new File(STORE_FOLDER, fileName + FILE_EXTN)));
    }

    private String getRecIdFromKey(String key) {
        if (recordKeyIdMap.containsKey(key)) {
            return recordKeyIdMap.get(key);
        } else {
            return null;
        }

    }

    private String getFirstPartOfPair(String row) {
        return row.substring(0, row.indexOf(DELIMITER)).trim();
    }

    private String getLstPartOfPair(String row) {
        return row.substring(row.indexOf(DELIMITER) + 1).trim();
    }

    private void saveListDataToFile(List<String> dataList, String fileName, String header) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(new File(STORE_FOLDER, fileName + FILE_EXTN)))) {
            int i = 0;
            bw.write(header);
            bw.write("\n");
            for (String row : dataList) {
                bw.write(row);
                bw.write("\n");
                if (i++ > ROW_COUNT_FLUSH) {
                    bw.flush();
                    i = 0;
                }
            }
            bw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
