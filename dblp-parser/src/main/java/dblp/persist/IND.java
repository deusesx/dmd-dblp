package dblp.persist;

final class IND {

    private IND(){};

    final static int IND_THESIS, START_ID, NOT_FOUND_ID, IND_AUTHOR, IND_PUBLISHER, IND_EDITOR, IND_JOURNAL,
            IND_CITE, IND_CROSSREF, IND_ISBN, IND_URL, IND_EE, IND_NOTE, COUNT_STANDALONE, COUNT_ITEM, COUNT_ENTITY,
            IND_RECORD, IND_ARTICLE, IND_BOOK,  IND_INCLUSION, IND_SERIES, IND_SCHOOL, COUNT_FK;

    static {
        START_ID = 1;
        NOT_FOUND_ID = -1;
        //standalone
        COUNT_STANDALONE = 8;
        IND_AUTHOR = 0;
        IND_EDITOR = 1;
        IND_PUBLISHER = 2;
        IND_CITE = 3;
        IND_JOURNAL = 4;
        IND_CROSSREF = 5;
        IND_SERIES = 6;
        IND_SCHOOL = 7;
        //fk
        COUNT_FK = 4;
        //items
        COUNT_ITEM = 4;
        IND_ISBN = 0;
        IND_URL = 1;
        IND_EE = 2;
        IND_NOTE = 3;
        //entities
        COUNT_ENTITY = 5;
        IND_INCLUSION = 0;
        IND_BOOK = 1;
        IND_THESIS = 2;
        IND_ARTICLE = 3;
        IND_RECORD = 4;
    }

}
