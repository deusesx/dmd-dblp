package dblp.utils;

import java.io.IOException;
import java.util.Properties;

/**
 * Uniq Properties
 */
public class Prop {
    private static Properties parserProperties;

    private Prop() {}


    public static String getProperty(String key) {
        if (parserProperties == null) {
            parserProperties = new Properties();
            try {
                parserProperties.load(Prop.class.getResourceAsStream("/dblp/utils/dblp.properties"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return parserProperties.getProperty(key);
    }

}