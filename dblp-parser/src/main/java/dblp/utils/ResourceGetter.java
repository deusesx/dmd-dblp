package dblp.utils;

import java.io.IOException;
import java.io.InputStream;

public final class ResourceGetter {
    private ResourceGetter(){}

    /**
     * @param fileName
     * @return File associated by filename in resource's folder
     * @throws IOException
     */
    public static InputStream getResStream(String fileName){
        System.out.println(ResourceGetter.class.getResource(fileName));
        return ResourceGetter.class.getResourceAsStream(fileName);
    }
}
