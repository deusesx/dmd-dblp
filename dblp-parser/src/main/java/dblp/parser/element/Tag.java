package dblp.parser.element;

import dblp.parser.schema.*;

import java.util.HashMap;
import java.util.Map;

public enum Tag {
    ARTICLE(Article.class),
    INPROCEEDINGS(Inproceedings.class),
    PROCEEDINGS(Proceedings.class),
    BOOK(Book.class),
    INCOLLECTION(Incollection.class),
    PHDTHESIS(Phdthesis.class),
    MASTERSTHESIS(Mastersthesis.class),
    WWW(Www.class);

    private static final Map<String, Tag> classToTagMap;
    private final Class type;

    static {
        classToTagMap = new HashMap<>();
        for (Tag tag : values())
            classToTagMap.put(tag.type.getName(), tag);
    }

    Tag(Class type) {
        this.type = type;
    }

    public Class type() {
        return type;
    }

    public static Tag getTag(String className) {
        return (classToTagMap.containsKey(className)) ? classToTagMap.get(className) : null;
    }

}