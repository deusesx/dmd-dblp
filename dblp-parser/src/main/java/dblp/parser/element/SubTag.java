package dblp.parser.element;

import dblp.parser.schema.*;

public enum SubTag {
    AUTHOR(Author.class),
    EDITOR(Editor.class),
    TITLE(Title.class),
    PAGES(Pages.class),
    YEAR(Year.class),
    ADDRESS(Address.class),
    JOURNAL(Journal.class),
    VOLUME(Volume.class),
    NUMBER(dblp.parser.schema.Number.class),
    MONTH(Month.class),
    URL(Url.class),
    EE(Ee.class),
    CHAPTER(Chapter.class),
    BOOKTITLE(Booktitle.class),
    CITE(Cite.class),
    PUBLISHER(Publisher.class),
    NOTE(Note.class),
    CROSSREF(Crossref.class),
    ISBN(Isbn.class),
    SERIES(Series.class),
    SCHOOL(School.class),
    CDROM(Cdrom.class);

    private final Class type;

    SubTag(Class type) {
        this.type = type;
    }

    public Class type(){return  type;}
}
