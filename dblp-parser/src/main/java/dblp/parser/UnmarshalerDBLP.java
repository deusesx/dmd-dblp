package dblp.parser;

import dblp.parser.element.Tag;
import dblp.parser.entity_parser.InspectorFactory;
import dblp.parser.entity_parser.RecordInsp;
import dblp.parser.schema.Dblp;
import dblp.persist.Container;
import dblp.persist.MergerFK;
import dblp.persist.PersistContainer;
import dblp.utils.Prop;
import dblp.utils.ResourceGetter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Source;
import javax.xml.transform.sax.SAXSource;
import java.io.*;

public class UnmarshalerDBLP {
    private static final Logger logger = LoggerFactory.getLogger(UnmarshalerDBLP.class);
    private final InspectorFactory inspectorFactory = new InspectorFactory();

    public void startParsing() {
        logger.info("Start parsing: {}", Prop.getProperty("dblp.parser.data_file"));
//        try {
//            Dblp dblp = initAndParse();
//            logger.info("End parsing. Start inspection DBLP entries.");
//            inspectAll(dblp);
//            finishPersistData();
            mergeFK();
//        } catch (FileNotFoundException | SAXException | UnsupportedEncodingException | JAXBException e) {
//            e.printStackTrace();
//        }
    }

    private void mergeFK(){
        new MergerFK().mergeAllFK();
    }

    private void finishPersistData(){
        Container.getSingleton().persistAllData();
    }

    private Dblp initAndParse() throws JAXBException, SAXException, FileNotFoundException, UnsupportedEncodingException {
        String SP_GENEAL_ENTITY_SIZE_LIMIT = "jdk.xml.entityExpansionLimit";
        //set limits using system property
        System.setProperty(SP_GENEAL_ENTITY_SIZE_LIMIT, "40000000");
        JAXBContext ctx = JAXBContext.newInstance("dblp.parser.schema");
        //jdk.xml.entityExpansionLimit = 50000000
        Unmarshaller unmarshaller = ctx.createUnmarshaller();
        XMLReader xmlreader = XMLReaderFactory.createXMLReader();
        xmlreader.setEntityResolver(new EntityResolver() {
            public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
                return new InputSource(ResourceGetter.getResStream(Prop.getProperty("dblp.parser.dtd_file")));
            }
        });
        InputSource input = convertToUTF8(Prop.getProperty("dblp.parser.data_file"));
        Source source = new SAXSource(xmlreader, input);
        return (Dblp) unmarshaller.unmarshal(source);
    }

    private void inspectAll(Dblp dblp) {
        for (Object entry : dblp.getArticleOrInproceedingsOrProceedingsOrBookOrIncollectionOrPhdthesisOrMastersthesisOrWww()) {
            inspectEntry(entry);
        }
    }

    private void inspectEntry(Object entry) {
        for (Tag tag : Tag.values()) {
            if (tag.type().isInstance(entry)) {
                inspectorFactory.inspectRecord(tag, entry);
                break;
            }
        }

    }

    private InputSource convertToUTF8(String fileName) throws FileNotFoundException, UnsupportedEncodingException {
        Reader reader = new InputStreamReader(new FileInputStream(fileName), "UTF-8");
        InputSource is = new InputSource(reader);
        is.setEncoding("UTF-8");
        return is;
    }
}
