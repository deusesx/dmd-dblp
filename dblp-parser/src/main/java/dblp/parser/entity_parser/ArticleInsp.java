package dblp.parser.entity_parser;

import dblp.model.RecordArticle;
import dblp.parser.element.SubTag;
import dblp.parser.element.Tag;
import dblp.parser.schema.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class ArticleInsp extends RecordInsp {

    private Article curArticle;
    private RecordArticle curRecArticle;

    @Override
    void initEntity(int recordId) {
        curRecArticle = container.createArticle(recordId);
    }

    @Override
    String[] inspectEntityAndGetRecAttr(Object entity) {
        curArticle = (Article) entity;
        List<Object> itemsList = curArticle.getAuthorOrEditorOrTitleOrBooktitleOrPagesOrYearOrAddressOrJournalOrVolumeOrNumberOrMonthOrUrlOrEeOrCdromOrCiteOrPublisherOrNoteOrCrossrefOrIsbnOrSeriesOrSchoolOrChapter();
        String title = inspectItemsAndGetTitle(itemsList);
        String[] recAttr = getRecAttr(title);
        container.addArticle(curRecArticle);
        debug(curRecArticle);
        return recAttr;
    }

    /**
     * Copy that method to every instance of RecordInsp
     */
    String[] getRecAttr(String title) {
        String[] recAttr = new String[3];
        recAttr[RecordInsp.IND_KEY] = curArticle.getKey();
        recAttr[RecordInsp.IND_MDATE] = curArticle.getMdate();
        recAttr[RecordInsp.IND_TITLE] = title;
        return recAttr;
    }

    @Override
    Tag getRecordType() {
        return Tag.getTag(Article.class.getName());
    }

    String inspectItemsAndGetTitle(List<Object> itemsList) {
        String title = "";
        for (Object item : itemsList) {
            for (SubTag subTag : SubTag.values()) {
                if (subTag.type().isInstance(item)) {
                    switch (subTag) {
                        case TITLE:
                            title = ((Title) item).getvalue();
                            break;
                        case PAGES:
                            curRecArticle.setPages(((Pages) item).getvalue());
                            break;
                        case VOLUME:
                            curRecArticle.setVolume(((Volume) item).getvalue());
                            break;
                        case NUMBER:
                            curRecArticle.setNum(((dblp.parser.schema.Number) item).getvalue());
                            break;
                        case YEAR:
                            curRecArticle.setYear(Integer.valueOf(((Year) item).getvalue()));
                            break;
                        case JOURNAL:
                            curRecArticle.setJournalId(container.getJournalId(item));
                            break;
                        default:
                            inspectRecordItems(subTag, item);
                    }
                    break;
                }
            }
        }
        return title;
    }

}
