package dblp.parser.entity_parser;

import dblp.model.RecordThesis;
import dblp.parser.element.SubTag;
import dblp.parser.element.Tag;
import dblp.parser.schema.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class ThesisInsp extends RecordInsp {
    private RecordThesis curRecordThesis;
    public final static int TYPE_PHD = 0, TYPE_MS = 1;
    private int typeInsp;

    private ThesisInsp() {
    }

    public ThesisInsp(int typeInsp) {
        this.typeInsp = typeInsp;
    }

    @Override
    void initEntity(int recordId) {
        curRecordThesis = container.createThesis(recordId);
    }

    @Override
    String[] inspectEntityAndGetRecAttr(Object entity) {
        String[] recAttr = new String[3];
        if (typeInsp == TYPE_PHD) {
            curRecordThesis.setType(RecordThesis.THESIS_TYPE.phd);
            Phdthesis curThesis = (Phdthesis) entity;
            List<Object> itemsList = curThesis.getAuthorOrEditorOrTitleOrBooktitleOrPagesOrYearOrAddressOrJournalOrVolumeOrNumberOrMonthOrUrlOrEeOrCdromOrCiteOrPublisherOrNoteOrCrossrefOrIsbnOrSeriesOrSchoolOrChapter();
            recAttr[RecordInsp.IND_KEY] = curThesis.getKey();
            recAttr[RecordInsp.IND_MDATE] = curThesis.getMdate();
            recAttr[RecordInsp.IND_TITLE] = inspectItemsAndGetTitle(itemsList);
        } else if (typeInsp == TYPE_MS) {
            curRecordThesis.setType(RecordThesis.THESIS_TYPE.master);
            Mastersthesis curThesis = (Mastersthesis) entity;
            List<Object> itemsList = curThesis.getAuthorOrEditorOrTitleOrBooktitleOrPagesOrYearOrAddressOrJournalOrVolumeOrNumberOrMonthOrUrlOrEeOrCdromOrCiteOrPublisherOrNoteOrCrossrefOrIsbnOrSeriesOrSchoolOrChapter();
            recAttr[RecordInsp.IND_KEY] = curThesis.getKey();
            recAttr[RecordInsp.IND_MDATE] = curThesis.getMdate();
            recAttr[RecordInsp.IND_TITLE] = inspectItemsAndGetTitle(itemsList);
        }
        container.addThesis(curRecordThesis);
        debug(curRecordThesis);
        return recAttr;
    }

    @Override
    Tag getRecordType() {
        return (typeInsp == TYPE_PHD) ? Tag.getTag(Phdthesis.class.getName()) : Tag.getTag(Mastersthesis.class.getName());
    }

    @Override
    String inspectItemsAndGetTitle(List<Object> itemsList) {
        String title = "";
        for (Object item : itemsList) {
            for (SubTag subTag : SubTag.values()) {
                if (subTag.type().isInstance(item)) {
                    switch (subTag) {
                        case TITLE:
                            title = ((Title) item).getvalue();
                            break;
                        case PAGES:
                            curRecordThesis.setPages(((Pages) item).getvalue());
                            break;
                        case YEAR:
                            curRecordThesis.setYear(Integer.valueOf(((Year) item).getvalue()));
                            break;
                        case SCHOOL:
                            curRecordThesis.setSchoolId(container.getSchoolId(item));
                            break;
                        default:
                            inspectRecordItems(subTag, item);
                    }
                    break;
                }
            }
        }
        return title;
    }
}
