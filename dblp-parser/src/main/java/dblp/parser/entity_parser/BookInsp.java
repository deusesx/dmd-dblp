package dblp.parser.entity_parser;

import dblp.model.RecordBook;
import dblp.parser.element.SubTag;
import dblp.parser.element.Tag;
import dblp.parser.schema.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class BookInsp extends RecordInsp { //ToDo check if it does work

    private RecordBook curRecBook;
    private int typeInsp;

    private BookInsp(){}

    public BookInsp(int typeInsp) {
        this.typeInsp = typeInsp;
    }
    public final static int TYPE_BOOK = 0, TYPE_PROCEEDING = 1;

    @Override
    void initEntity(int recordId) {
        curRecBook = container.createBook(recordId);
    }

    @Override
    String[] inspectEntityAndGetRecAttr(Object entity) {
        String[] recAttr = new String[3];
        if (typeInsp == TYPE_BOOK) {
            curRecBook.setType(RecordBook.BOOK_TYPE.book);
            Book curBook = (Book) entity;
            List<Object> itemsList = curBook.getAuthorOrEditorOrTitleOrBooktitleOrPagesOrYearOrAddressOrJournalOrVolumeOrNumberOrMonthOrUrlOrEeOrCdromOrCiteOrPublisherOrNoteOrCrossrefOrIsbnOrSeriesOrSchoolOrChapter();
            recAttr[RecordInsp.IND_TITLE] = inspectItemsAndGetTitle(itemsList);
            recAttr[RecordInsp.IND_KEY] = curBook.getKey();
            recAttr[RecordInsp.IND_MDATE] = curBook.getMdate();
        } else if (typeInsp == TYPE_PROCEEDING) {
            curRecBook.setType(RecordBook.BOOK_TYPE.proceeding);
            Proceedings curBook = (Proceedings) entity;
            List<Object> itemsList = curBook.getAuthorOrEditorOrTitleOrBooktitleOrPagesOrYearOrAddressOrJournalOrVolumeOrNumberOrMonthOrUrlOrEeOrCdromOrCiteOrPublisherOrNoteOrCrossrefOrIsbnOrSeriesOrSchoolOrChapter();
            recAttr[RecordInsp.IND_TITLE] = inspectItemsAndGetTitle(itemsList);
            recAttr[RecordInsp.IND_KEY] = curBook.getKey();
            recAttr[RecordInsp.IND_MDATE] = curBook.getMdate();
        }
        container.addBook(curRecBook);
        debug(curRecBook);
        return recAttr;
    }

    @Override
    Tag getRecordType() {
        return Tag.getTag(Book.class.getName());
    }

    @Override
    String inspectItemsAndGetTitle(List<Object> itemsList) {
        String title = "";
        for (Object item : itemsList) {
            for (SubTag subTag : SubTag.values()) {
                if (subTag.type().isInstance(item)) {
                    switch (subTag) {
                        case TITLE:
                            title = ((Title) item).getvalue();
                            break;
                        case VOLUME:
                            curRecBook.setVolume(((Volume) item).getvalue());
                            break;
                        case YEAR:
                            curRecBook.setYear(Integer.valueOf(((Year) item).getvalue()));
                            break;
                        case BOOKTITLE:
                            curRecBook.setBooktitle(((Booktitle) item).getvalue());
                            break;
                        case SERIES:
                            curRecBook.setSeriesId(container.getSeriesId(item));
                            break;
                        default:
                            inspectRecordItems(subTag, item);
                    }
                    break;
                }
            }
        }
        return title;
    }

}
