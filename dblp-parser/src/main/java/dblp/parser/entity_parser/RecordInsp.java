package dblp.parser.entity_parser;

import dblp.model.Record;
import dblp.parser.element.SubTag;
import dblp.parser.element.Tag;
import dblp.persist.Container;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static dblp.model.Record.REC_TYPE;

public abstract class RecordInsp {

    private static final Logger logger = LoggerFactory.getLogger(RecordInsp.class);
    private static final Map<Tag, Record.REC_TYPE> tagTypeMap;
    protected static final int IND_KEY = 0, IND_MDATE = 1, IND_TITLE = 2;
    public static final Container container;
    private Record curRecord;

    static {
        container = Container.getSingleton();
        tagTypeMap = new HashMap<>();
        // Schema and model integration (used only there)
        tagTypeMap.put(Tag.ARTICLE, REC_TYPE.article);
        tagTypeMap.put(Tag.BOOK, REC_TYPE.book);
        tagTypeMap.put(Tag.INCOLLECTION, REC_TYPE.incollection);
        tagTypeMap.put(Tag.INPROCEEDINGS, REC_TYPE.inproceedings);
        tagTypeMap.put(Tag.MASTERSTHESIS, REC_TYPE.msthesis);
        tagTypeMap.put(Tag.PHDTHESIS, REC_TYPE.phdthesis);
        tagTypeMap.put(Tag.PROCEEDINGS, REC_TYPE.proceedings);
    }

    final void debug(Object obj) {
        if (logger.isDebugEnabled()) {
            logger.debug(obj.toString());
        }
    }

    public final void inspect(Object entity) {
        initRecord();
        initEntity(curRecord.getRecordId());
        setRecordType();
        String[] keyMdateTitle = inspectEntityAndGetRecAttr(entity);
        inspectRecord(keyMdateTitle);
        container.addRecord(curRecord);
        debug(curRecord);
        cleanState();
    }

    private void cleanState(){
        curRecord = null;
        container.cleanCheckCite();
    }

    private void initRecord() {
        curRecord = container.createRecord();
    }

    private void setRecordType() {
        Tag tag = getRecordType();
        curRecord.setType(tagTypeMap.get(tag));
    }

    abstract void initEntity(int recordId);

    private void inspectRecord(String[] keyMdateTitle) {
        curRecord.setKey(keyMdateTitle[IND_KEY]);
        curRecord.setMdate(keyMdateTitle[IND_MDATE]);
        curRecord.setTitle(keyMdateTitle[IND_TITLE]);
    }

    protected void inspectRecordItems(SubTag subTag, Object item) {
        switch (subTag) {
            //items
            case URL:
                container.addUrl(item, curRecord.getRecordId());
                break;
            case EE:
                container.addEE(item, curRecord.getRecordId());
                break;
            case NOTE:
                container.addNote(item, curRecord.getRecordId());
                break;
            case ISBN:
                container.addISBL(item, curRecord.getRecordId());
                break;
            //standalone
            case AUTHOR:
                container.addAuthor(item, curRecord.getRecordId());
                break;
            case PUBLISHER:
                container.addPublisher(item, curRecord.getRecordId());
                break;
            case EDITOR:
                container.addEditor(item, curRecord.getRecordId());
                break;
            case CITE:
                container.addCite(item, curRecord.getRecordId());
                break;
        }
    }


    abstract String[] inspectEntityAndGetRecAttr(Object entity);

    abstract Tag getRecordType();

    abstract String inspectItemsAndGetTitle(List<Object> itemsList);


}
