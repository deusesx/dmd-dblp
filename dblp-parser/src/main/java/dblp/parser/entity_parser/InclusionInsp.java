package dblp.parser.entity_parser;

import dblp.model.RecordInclusion;
import dblp.parser.element.SubTag;
import dblp.parser.element.Tag;
import dblp.parser.schema.*;

import java.util.List;

public class InclusionInsp extends RecordInsp {
    private RecordInclusion curRecordInclusion;
    public final static int TYPE_INCOLLECTION = 0, TYPE_INPROCEEDINGS = 1;
    private int typeInsp;

    private InclusionInsp() {
    }

    public InclusionInsp(int type) {
        typeInsp = type;
    }

    @Override
    void initEntity(int recordId) {
        curRecordInclusion = container.createIncollectionOrInproceedings(recordId);
    }

    /**
     * It looks like fucked code because I can not add common interface to generated code.
     */
    @Override
    String[] inspectEntityAndGetRecAttr(Object entity) {
        String[] recAttr = new String[3];
        if (typeInsp == TYPE_INCOLLECTION) {
            curRecordInclusion.setType(RecordInclusion.INCLUSION_TYPE.incollection);
            Incollection incollection = (Incollection) entity;
            List<Object> itemsList = incollection.getAuthorOrEditorOrTitleOrBooktitleOrPagesOrYearOrAddressOrJournalOrVolumeOrNumberOrMonthOrUrlOrEeOrCdromOrCiteOrPublisherOrNoteOrCrossrefOrIsbnOrSeriesOrSchoolOrChapter();
            recAttr[RecordInsp.IND_KEY] = incollection.getKey();
            recAttr[RecordInsp.IND_MDATE] = incollection.getMdate();
            recAttr[RecordInsp.IND_TITLE] = inspectItemsAndGetTitle(itemsList);
        } else if (typeInsp == TYPE_INPROCEEDINGS) {
            curRecordInclusion.setType(RecordInclusion.INCLUSION_TYPE.inproceedings);
            Inproceedings inproceedings = (Inproceedings) entity;
            List<Object> itemsList = inproceedings.getAuthorOrEditorOrTitleOrBooktitleOrPagesOrYearOrAddressOrJournalOrVolumeOrNumberOrMonthOrUrlOrEeOrCdromOrCiteOrPublisherOrNoteOrCrossrefOrIsbnOrSeriesOrSchoolOrChapter();
            recAttr[RecordInsp.IND_KEY] = inproceedings.getKey();
            recAttr[RecordInsp.IND_MDATE] = inproceedings.getMdate();
            recAttr[RecordInsp.IND_TITLE] = inspectItemsAndGetTitle(itemsList);
        }
        container.addInclusion(curRecordInclusion);
        debug(curRecordInclusion);
        return recAttr;
    }

    @Override
    Tag getRecordType() {
        return (typeInsp == TYPE_INCOLLECTION) ? Tag.getTag(Incollection.class.getName()) : Tag.getTag(Inproceedings.class.getName());
    }

    @Override
    String inspectItemsAndGetTitle(List<Object> itemsList) {
        String title = "";
        for (Object item : itemsList) {
            for (SubTag subTag : SubTag.values()) {
                if (subTag.type().isInstance(item)) {
                    switch (subTag) {
                        case TITLE:
                            title = ((Title) item).getvalue();
                            break;
                        case BOOKTITLE:
                            curRecordInclusion.setBookTitle(((Booktitle) item).getvalue());
                            break;
                        case PAGES:
                            curRecordInclusion.setPages(((Pages) item).getvalue());
                            break;
                        case YEAR:
                            curRecordInclusion.setYear(Integer.valueOf(((Year) item).getvalue()));
                            break;
                        case CROSSREF:
                            curRecordInclusion.setCrossRefId(container.getCrossRefId(item));
                            break;
                        default:
                            inspectRecordItems(subTag, item);
                    }
                    break;
                }
            }
        }
        return title;
    }
}
