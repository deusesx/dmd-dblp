package dblp.parser.entity_parser;

import dblp.parser.element.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class InspectorFactory {
    private static final Logger logger = LoggerFactory.getLogger(InspectorFactory.class);

    private final Map<Tag, RecordInsp> inspectorMap;

    public InspectorFactory() {
        inspectorMap = new HashMap<>();
        initInspectorMap();
    }

    private void initInspectorMap() {
        inspectorMap.put(Tag.ARTICLE, new ArticleInsp());
        inspectorMap.put(Tag.BOOK, new BookInsp(BookInsp.TYPE_BOOK));
        inspectorMap.put(Tag.PROCEEDINGS, new BookInsp(BookInsp.TYPE_PROCEEDING));
        inspectorMap.put(Tag.MASTERSTHESIS, new ThesisInsp(ThesisInsp.TYPE_MS));
        inspectorMap.put(Tag.PHDTHESIS, new ThesisInsp(ThesisInsp.TYPE_PHD));
        inspectorMap.put(Tag.INCOLLECTION, new InclusionInsp(InclusionInsp.TYPE_INCOLLECTION));
        inspectorMap.put(Tag.INPROCEEDINGS, new InclusionInsp(InclusionInsp.TYPE_INPROCEEDINGS));
    }

    public void inspectRecord(Tag tag, Object entity) {
        RecordInsp inspector = inspectorMap.get(tag); //ToDo check tag null
        if (inspector != null) {
            inspector.inspect(entity);
        } else {
            logger.warn("for tag: {} there is no inspector class (null)", tag);
        }
    }

}
