package dblp.parser.entity_parser;

import dblp.utils.Prop;

public class StructForeigns {

    public int entityId;
    public int foreignId;
    private final static String DELIMITER = Prop.getProperty("dblp.parser.delimiter");

    public StructForeigns(int entityId, int foreignId) {
        this.entityId = entityId;
        this.foreignId = foreignId;
    }

    /**
     * Be aware of delimiter for lightweight of object
     */
    @Override
    public String toString() {
        return entityId + DELIMITER + foreignId;

    }
}
