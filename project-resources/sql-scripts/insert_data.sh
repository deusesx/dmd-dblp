#!/bin/bash
# psql < createDB.sql
FOLDER=data
EXT=csv
DB_NAME=dblp
FILES=$FOLDER/*.$EXT
for f in $FILES
do
  f=${f##*/}
  filename=${f%.*}
  echo "Processing $filename file..."
  psql $DB_NAME -c "\COPY public.$filename FROM $FOLDER/$filename.$EXT DELIMITER ',' CSV;"
done
exit
