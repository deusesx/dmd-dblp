CORRIDA project.
General Information and Instruction.
This document contains the following sections:
    * Web-app installation guid
    * Database installation
    * Communication server execution

I. Web-app installation guid.
      1) Install Python 2.7
      2) Install all libraries (pip install *):
	- django, registration
	- psycopg2 - driver to connect database
	- go to folder dblp-webapp
	- python manage.py migrate
	- python manage.py runserver
      3) After that we can go to 127.0.0.1:8000 and see our web-app
      4) Main user story:
	- user go web-site
	- user registrate to the web-site
	- after, to his email send an email with confirmation link
	- On the main page he can search and create new publication
	- search results displayed 10 per page
	- each result (paper record) we can update or/and delete  
	- Also, for each result we can see the advance information about article,
	- And also related articles - another papers from authors, and also cited in this paper papers
	- Also user can see the papers, which cire this current paper

II. Database installation
      1)  Install the Java SDK, level 1.7 or higher.

      2) The database project is placed in the dblp-database folder.

      2)  If you do install Java 1.5, you need to make some minor changes 
          to the file dblp_database.properties:
          database_folder= set folder for your database files
          database_name= set name of the database

      4)  Add that folder to your classpath.

      5) Execute corridadb.serverStartup class

III. Communication server execution
      1) Install the Java SDK, level 1.7 or higher.
      2) Add dblp-database folder to your classpath.
      3) Execute dblpclient.StartServer
