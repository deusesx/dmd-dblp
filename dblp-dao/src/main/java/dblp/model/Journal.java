package dblp.model;

public class Journal {

    private int id;
    private String name;
    private java.util.Set<RecordArticle> articles;  //ToDo think about nullable

    private void setId(int value) {
        this.id = value;
    }

    public int getId() {
        return id;
    }

    public void setName(String value) {
        this.name = value;
    }

    public String getName() {
        return name;
    }

    public void setArticles(java.util.Set value) {
        this.articles = value;
    }

    public java.util.Set getArticles() {
        return articles;
    }

}
