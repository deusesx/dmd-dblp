package dblp.model;

public class Record {

    public static enum REC_TYPE {
        article, book, incollection, inproceedings, msthesis, phdthesis, proceedings
    }

    private final static String OUT_FORMAT = "%d,%s,%s,%s,%s".replace(",", Delimiter.DELIMITER);
    private int recordId;
    private String key;
    private String mdate;
    private String title;
    private REC_TYPE type;

    private java.util.Set editor = new java.util.HashSet();
    private java.util.Set publisher = new java.util.HashSet();
    private java.util.Set author = new java.util.HashSet();
    private RecordArticle recordArticle;
    private RecordBook recordBook;
    private java.util.Set entity_citation = new java.util.HashSet();
    private Record_ee record_ee;
    private RecordInclusion recordInclusion;
    private java.util.Set dblp_entity_incollection1 = new java.util.HashSet();
    private java.util.Set dblp_entity_inproceedings1 = new java.util.HashSet();
    private java.util.Set dblp_entity_isbn = new java.util.HashSet();
    private Record_note record_note;
    //ToDo think about this generated content, also already deleted THESIS, Inclusions, proceedings
    private Record_url record_url;

    public Record setRecordId(int value) {
        this.recordId = value;
        return this;
    }

    public int getRecordId() {
        return recordId;
    }

    public void setKey(String value) {
        this.key = value;
    }

    public String getKey() {
        return key;
    }

    public void setMdate(String value) {
        this.mdate = value;
    }

    public String getMdate() {
        return mdate;
    }

    public void setTitle(String value) {
        this.title = value;
    }

    public String getTitle() {
        return title;
    }

    public void setType(REC_TYPE value) {
        this.type = value;
    }

    public REC_TYPE getType() {
        return type;
    }

    //--------------------------------------------------------

    public void setEditor(java.util.Set value) {
        this.editor = value;
    }

    public java.util.Set getEditor() {
        return editor;
    }

    public void setPublisher(java.util.Set value) {
        this.publisher = value;
    }

    public java.util.Set getPublisher() {
        return publisher;
    }

    public void setAuthor(java.util.Set value) {
        this.author = value;
    }

    public java.util.Set getAuthor() {
        return author;
    }

    public void setRecordArticle(RecordArticle value) {
        this.recordArticle = value;
    }

    public RecordArticle getRecordArticle() {
        return recordArticle;
    }

    public void setRecordBook(RecordBook value) {
        this.recordBook = value;
    }

    public RecordBook getRecordBook() {
        return recordBook;
    }

    public void setEntity_citation(java.util.Set value) {
        this.entity_citation = value;
    }

    public java.util.Set getEntity_citation() {
        return entity_citation;
    }

    public void setRecord_ee(Record_ee value) {
        this.record_ee = value;
    }

    public Record_ee getRecord_ee() {
        return record_ee;
    }

    public void setRecordInclusion(RecordInclusion value) {
        this.recordInclusion = value;
    }

    public RecordInclusion getRecordInclusion() {
        return recordInclusion;
    }

    public void setDblp_entity_incollection1(java.util.Set value) {
        this.dblp_entity_incollection1 = value;
    }

    public java.util.Set getDblp_entity_incollection1() {
        return dblp_entity_incollection1;
    }


    public void setDblp_entity_inproceedings1(java.util.Set value) {
        this.dblp_entity_inproceedings1 = value;
    }

    public java.util.Set getDblp_entity_inproceedings1() {
        return dblp_entity_inproceedings1;
    }

    public void setDblp_entity_isbn(java.util.Set value) {
        this.dblp_entity_isbn = value;
    }

    public java.util.Set getDblp_entity_isbn() {
        return dblp_entity_isbn;
    }


    public void setRecord_note(Record_note value) {
        this.record_note = value;
    }

    public Record_note getRecord_note() {
        return record_note;
    }


    public void setRecord_url(Record_url value) {
        this.record_url = value;
    }

    public Record_url getRecord_url() {
        return record_url;
    }

    @Override
    public String toString() {
        return String.format(OUT_FORMAT, recordId, key, mdate, title, type);
    }

}
