package dblp.model;

public abstract class NameEntityAbstracton {

    private int id;
    private String name;
    private java.util.Set entitySet = new java.util.HashSet();

    private void setId(int value) {
        this.id = value;
    }

    public int getId() {
        return id;
    }

    public void setName(String value) {
        this.name = value;
    }

    public String getName() {
        return name;
    }

    public void setEntitySet(java.util.Set value) {
        this.entitySet = value;
    }

    public java.util.Set getEntitySet() {
        return entitySet;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

}
