package dblp.model;

import java.util.List;

public abstract class RecordEntityAbstract {

    private Record record;
    private int entityId;
    private Integer year;
    private List<Author> authors;

    public RecordEntityAbstract(int entityId) {
        this.entityId = entityId;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public void setEntityId(int value) {
        this.entityId = value;
    }

    public int getEntityId() {
        return entityId;
    }

    public void setRecord(Record value) {
        this.record = value;
    }

    public Record getRecord() {
        return record;
    }

    public void setYear(Integer value) {
        this.year = value;
    }

    public Integer getYear() {
        return year;
    }

    @Override
    public String toString() {
        return String.valueOf(entityId);
    }
}