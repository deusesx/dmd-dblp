package dblp.model;

public class RecordArticle extends RecordEntityAbstract {

    private final static String OUT_FORMAT =",%s,%s,%d,%s,%s".replace(",", Delimiter.DELIMITER);
    private Record_school school;
    private String num;
    private String pages;
    private String volume;
    private Integer journalId;
    private Journal journal;
    /**
     * ee: is many-to-one. Delimiter is ';' if it has more than one
     */
    private String ee;

    public RecordArticle(int entityId) {
        super(entityId);
    }

    public String getEe() {
        return ee;
    }

    public void setEe(String ee) {
        this.ee = ee;
    }

    public void setPages(String value) {
        this.pages = value;
    }

    public String getPages() {
        return pages;
    }

    public void setVolume(String value) {
        this.volume = value;
    }

    public String getVolume() {
        return volume;
    }

    public void setNum(String value) {
        this.num = value;
    }

    public String getNum() {
        return num;
    }

    public void setJournal(Journal value) {
        this.journal = value;
    }

    public void setJournalId(Integer journalId) {

        this.journalId = journalId;
    }

    public Journal getJournal() {
        return journal;
    }

    @Override
    public String toString() {
        return super.toString() + String.format(OUT_FORMAT, pages, volume, getYear(), journalId, num);
    }
}
