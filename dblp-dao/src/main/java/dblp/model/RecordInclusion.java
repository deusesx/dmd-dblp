package dblp.model;

public class RecordInclusion extends RecordEntityAbstract implements Typeable<RecordInclusion.INCLUSION_TYPE>{

    private final static String OUT_FORMAT =",%s,%s,%s,%s,%d".replace(",", Delimiter.DELIMITER);
    private String bookTitle;
    private Record crossRef;
    private int crossRefId;
    private String pages;
    private INCLUSION_TYPE type;

    public static enum INCLUSION_TYPE {
        incollection, inproceedings
    }

    public void setCrossRefId(int crossRefId) {
        this.crossRefId = crossRefId;
    }

    public RecordInclusion(int entityId) {
        super(entityId);
    }

    public void setBookTitle(String value) {
        this.bookTitle = value;
    }

    public String getBookTitle() {
        return bookTitle;
    }

    public void setPages(String value) {
        this.pages = value;
    }

    public String getPages() {
        return pages;
    }

    public void setCrossRef(Record record) {
        this.crossRef = record;
    }

    public Record getCrossRef() {
        return crossRef;
    }

    @Override
    public void setType(INCLUSION_TYPE typeName) {
        type = typeName;
    }

    @Override
    public String toString() {
        return super.toString() + String.format(OUT_FORMAT, type, bookTitle, pages, getYear(), crossRefId);
    }
}
