package dblp.model;

public class Record_note extends Record_itemAbstract implements Typeable<String>{

    public static enum NOTE_TYPE {
        urn,
        unicode,
        name,
        dnb,
        issn,
        award,
        isbn,
        affiliation,
        undefined //ToDO think about it
    }

    private NOTE_TYPE type;

    public NOTE_TYPE getType() {
        return type;
    }

    public void setType(String typeName) {
        if (hasType(typeName)) {
            this.type = NOTE_TYPE.valueOf(typeName);
        } else {
            this.type = NOTE_TYPE.undefined;
        }
    }

    public boolean hasType(String value) {
        for (NOTE_TYPE c : NOTE_TYPE.values()) {
            if (c.name().equals(value)) {
                return true;
            }
        }
        return false;
    }

}
