package dblp.model;

public class RecordThesis extends RecordEntityAbstract implements Typeable<RecordThesis.THESIS_TYPE>{

    private final static String OUT_FORMAT =",%s,%d,%s".replace(",", Delimiter.DELIMITER);
    private Record_school school;
    private Integer schoolId;  //ToDo think aout isbn
    private String pages;
    private THESIS_TYPE type;

    public static enum THESIS_TYPE {
        master, phd
    }

    public RecordThesis(int entityId) {
        super(entityId);
    }

    public void setSchool(Record_school school) {
        this.school = school;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    public Record_school getSchool() {
        return school;
    }


    public String getPages() {
        return pages;
    }

    public void setPages(String pages) {
        this.pages = pages;
    }

    @Override
    public void setType(THESIS_TYPE typeName) {
        type = typeName;
    }

    @Override
    public String toString() {
        return super.toString() + String.format(OUT_FORMAT, schoolId, getYear(), type);
    }

}
