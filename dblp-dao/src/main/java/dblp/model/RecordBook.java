package dblp.model;

import java.util.IntSummaryStatistics;

public class RecordBook extends RecordEntityAbstract implements Typeable<RecordBook.BOOK_TYPE>{

    private final static String OUT_FORMAT =",%s,%s,%d,%d,%s".replace(",", Delimiter.DELIMITER);
    private String booktitle;
    private String volume;
    private Record_series series;
    private Integer seriesId;
    private BOOK_TYPE type;

    public enum BOOK_TYPE{
        book, proceeding
    }

    public void setSeriesId(Integer seriesId) {
        this.seriesId = seriesId;
    }

    public void setSeries(Record_series series) {
        this.series = series;
    }

    private String href;

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public RecordBook(int entityId) {
        super(entityId);
    }

    public void setBooktitle(String value) {
        this.booktitle = value;
    }

    public String getBooktitle() {
        return booktitle;
    }

    public void setVolume(String value) {
        this.volume = value;
    }

    public String getVolume() {
        return volume;
    }

    @Override
    public void setType(BOOK_TYPE typeName) {
        type = typeName;
    }

    @Override
    public String toString() {
        return super.toString() + String.format(OUT_FORMAT, booktitle, volume, getYear(), seriesId, type); //ToDO check with database
    }

}
