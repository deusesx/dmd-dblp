package dblp.model;

public interface Typeable<T> {
    void setType(T typeName);
}
