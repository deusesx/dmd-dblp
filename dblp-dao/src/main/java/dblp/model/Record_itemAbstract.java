package dblp.model;

public abstract class Record_itemAbstract {

    private Record record;
    private int entityId;
    private String value;

    private void setEntityId(int value) {
        this.entityId = value;
    }

    public int getEntityId() {
        return entityId;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setRecord(Record value) {
        this.record = value;
    }

    public Record getRecord() {
        return record;
    }

}
