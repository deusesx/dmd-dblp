package dblp.dao;

import dblp.model.Record;

public interface RecordDAO {
    Record getRecordById(int id);

    int getCount();

    /**
     * Create new row in dblp_entity
     *
     * @return entity_id of inserted record
     */
    int createRecord(Record newRecord);
}
