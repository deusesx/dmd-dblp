from django.conf.urls import url
from dblp import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^greetings', views.beforelogin, name='greetings'),
    url(r'^advanced_search', views.advanced_search, name='advanced_search'),
    url(r'^beforelogin$', views.beforelogin, name='beforelogin'),
    url(r'^create_publication$', views.create_publication, name='create_publication'),
    url(r'^news$', views.news, name='news'),
    url(r'^article$', views.article, name='article'),
    url(r'^results$', views.results, name='results'),
    url(r'^results_id$', views.results_id, name='results_id'),
    url(r'^delete_result$', views.delete_result, name='delete_result'),
    url(r'^update_publication$', views.update_publication, name='update_publication'),
    url(r'^update_publication_result$', views.update_publication_result, name='update_publication_result'),
    url(r'^insert_entity_type$', views.insert_entity_type, name='insert_entity_type'),
    url(r'^insert_entity_type_result$', views.insert_entity_type_result, name='insert_entity_type_result'),
    url(r'^get_cited$',  views.get_cited, name='get_cited'),
    url(r'^get_cited_by$',  views.get_cited_by,  name='get_cited_by'),
    url(r'^get_related$',  views.get_related,  name='get_related'),
    url(r'^sort_id$',  views.sort_id,  name='sort_id'),
    url(r'^sort_title$',  views.sort_title,  name='sort_title'),
    url(r'^search_by_id$', views.search_by_id, name='search_by_id')

    # url(r'^(?P<question_id>[0-9]+)/results/$', views.results, name='results')
]
