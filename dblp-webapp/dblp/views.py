from django.http import HttpResponse
from django.template import loader, RequestContext
import time
from dblp_dao import dao_postgres, models, dao_corrida
from django.contrib.auth.decorators import login_required

# dao = dao_corrida
dao = dao_postgres
types = ['article', 'book', 'inclusion', 'thesis']


@login_required
def index(request):
    template = loader.get_template('news.html')
    context = RequestContext(request, {})
    return HttpResponse(template.render(context))


def beforelogin(request):
    template = loader.get_template('beforelogin.html')
    context = RequestContext(request, {

    })
    return HttpResponse(template.render(context))


@login_required
def advanced_search(request):
    template = loader.get_template('advanced_search.html')
    context = RequestContext(request, {

    })
    return HttpResponse(template.render(context))


@login_required
def create_publication(request):
    template = loader.get_template('create_publication.html')
    context = RequestContext(request, {

    })
    return HttpResponse(template.render(context))


@login_required
def news(request):
    template = loader.get_template('news.html')
    context = RequestContext(request, {

    })
    return HttpResponse(template.render(context))


@login_required
def results(request):
    template = loader.get_template('results.html')
    answer_type = ""
    if request.method == 'GET':
        answer_type = request.GET
    elif request.method == 'POST':
        answer_type = request.POST

    # entities_list = models.record
    title = ""
    title = answer_type['simple-search']
    entities = dao.get_entity_by_title(title)  #
    # paginator = Paginator(entities, 10) # Show 10 contacts per page
    # page = request.GET.get('page')
    # try:
    # entities_pages = paginator.page(page)
    # except PageNotAnInteger:
    # If page is not an integer, deliver first page.
    # entities_pages = paginator.page(1)
    # except EmptyPage:
    # If page is out of range (e.g. 9999), deliver last page of results.
    # entities_pages = paginator.page(paginator.num_pages)
    context = RequestContext(request, {"entities": entities, "title": title})
    return HttpResponse(template.render(context))

@login_required
def results_id(request):
    template = loader.get_template('results.html')
    answer_type = ""
    if request.method == 'GET':
        answer_type = request.GET
    elif request.method == 'POST':
        answer_type = request.POST

    id = int(answer_type['id'])
    entities = dao.get_entity_by_id(id)  #
    context = RequestContext(request, {"entities": entities})
    return HttpResponse(template.render(context))

@login_required
def delete_result(request):
    template = loader.get_template('delete_result.html')
    answer_type = ""
    if request.method == 'GET':
        answer_type = request.GET
    elif request.method == 'POST':
        answer_type = request.POST

    # entities_list = models.record
    val = answer_type['delete']
    val2 = answer_type['type']
    val = int(val)
    val2 = str(val2)

    dao.delete_entity_by_id(val)
    # sp_del_dblp_entity
    # entities = dao_postgres.get_entity_by_title(title)
    # paginator = Paginator(entities, 10) # Show 10 contacts per page
    # page = request.GET.get('page')
    # try:
    # entities_pages = paginator.page(page)
    # except PageNotAnInteger:
    # If page is not an integer, deliver first page.
    # entities_pages = paginator.page(1)
    # except EmptyPage:
    # If page is out of range (e.g. 9999), deliver last page of results.
    # entities_pages = paginator.page(paginator.num_pages)
    context = RequestContext(request, {"delete": val, "type": val2})
    return HttpResponse(template.render(context))

@login_required
def update_publication(request):
    template = loader.get_template('update_publication.html')
    answer_type = ""
    if request.method == 'GET':
        answer_type = request.GET
    elif request.method == 'POST':
        answer_type = request.POST

    # entities_list = models.record
    val = answer_type['update']
    val2 = answer_type['type']
    val = int(val)
    val2 = str(val2)
    article = ''
    book = ''
    inclusion = ''
    thesis = ''
    entity = dao.get_entity_by_id(val)
    time.sleep(2)
    if val2 in types:
        if val2 == types[0]:  # 'article'
            article = dao.get_article_by_id(val)
        elif val2 == types[1]:  # 'book'
            book = dao_postgres.get_book_by_id(val)
        elif val2 == types[2]:  # 'inclusion'
            inclusion = dao_postgres.get_inclusion_by_id(val)
        elif val2 == types[3]:  # 'thesis'
            thesis = dao_postgres.get_thesis_by_id(val)
        else:
            pass

    context = RequestContext(request, {"update": val, "type": val2, "entity": entity, "article": article, "book": book,
                                       "inclusion": inclusion, "thesis": thesis})
    return HttpResponse(template.render(context))

@login_required
def update_publication_result(request):
    template = loader.get_template('update_publication_result.html')
    answer_type = ""
    if request.method == 'GET':
        answer_type = request.GET
    elif request.method == 'POST':
        answer_type = request.POST

    # entities_list = models.record
    id = int(answer_type['id'])
    type = str(answer_type['type'])
    # Update title of entity by id
    new_title = str(answer_type['title'])
    # print(new_title)
    dao.update_entitiy_title(id, new_title)
    # if diff types - val2, then diff procedures
    if type in types:
        if type == types[0]:  # 'article'
            volume = str(answer_type['volume'])
            year = int(answer_type['year'])
            num = str(answer_type['num'])
            pages = str(answer_type['pages'])
            dao.update_entitiy_article(id, volume, year, num, pages)
        elif type == types[1]:  # 'book'
            booktitle = str(answer_type['booktitle'])
            volume = str(answer_type['volume'])
            year = int(answer_type['year'])
            dao_postgres.update_entitiy_book(id, booktitle, volume, year)
        elif type == types[2]:  # 'inclusion'

            booktitle = str(answer_type['booktitle'])
            year = int(answer_type['year'])
            pages = str(answer_type['pages'])

            dao_postgres.update_entitiy_inclusion(id, booktitle, year, pages)

        elif type == types[3]:  # 'thesis'
            year = int(answer_type['year'])
            dao_postgres.update_entitiy_thesis(id, year)
        else:
            pass
            # TODO article entity_id = 964261
            # book = 1338423
            # inclusion = 2820535
            # thesis = 1359112

    context = RequestContext(request, {"id": id, "type": type})
    return HttpResponse(template.render(context))


@login_required
def insert_entity_type(request):
    template = loader.get_template('insert_entity_type.html')
    answer_type = ""
    if request.method == 'GET':
        answer_type = request.GET
    elif request.method == 'POST':
        answer_type = request.POST

    title = str(answer_type['title'])
    type = str(answer_type['type'])

    if type not in types:
        type = 'article'

    id = dao.insert_entity(title, type)
    print(id)
    context = RequestContext(request, {"title": title, "type": type, "id": id})
    return HttpResponse(template.render(context))

@login_required
def insert_entity_type_result(request):
    template = loader.get_template('insert_entity_type_result.html')
    answer_type = ""
    if request.method == 'GET':
        answer_type = request.GET
    elif request.method == 'POST':
        answer_type = request.POST

    title = str(answer_type['title'])
    type = str(answer_type['type'])
    id = int(answer_type['id'])

    if type not in types:
        type = 'article'

    if type in types:
        if type == types[0]:  # 'article'
            volume = str(answer_type['volume'])
            if len(volume) == 0:
                volume = 'null'
            try:
                year = int(answer_type['year'])
            except:
                year = None

            num = str(answer_type['num'])

            if len(num) == 0:
                num = 'null'

            pages = str(answer_type['pages'])
            if len(pages) == 0:
                pages = 'null'

            dao.insert_article(id, volume, year, num, pages)

        elif type == types[1]:  # 'book'
            booktitle = str(answer_type['booktitle'])
            if len(booktitle) == 0:
                booktitle = 'null'

            volume = str(answer_type['volume'])
            if len(volume) == 0:
                volume = 'null'

            try:
                year = int(answer_type['year'])
            except:
                year = 'null'

            book_type = str(answer_type['book_type'])

            if book_type not in ['book', 'proceedings']:
                book_type = 'book'

            dao_postgres.insert_book(id, booktitle, volume, year, book_type)
        elif type == types[2]:  # 'inclusion'

            booktitle = str(answer_type['booktitle'])
            if len(booktitle) == 0:
                booktitle = 'null'

            try:
                year = int(answer_type['year'])
            except:
                year = 'null'

            pages = str(answer_type['pages'])
            if len(pages) == 0:
                pages = 'null'

            inclusion_type = str(answer_type['inclusion_type'])
            if inclusion_type not in ['inproceedings', 'incollection']:
                inclusion_type = 'inproceedings'

            dao_postgres.insert_inclusion(id, inclusion_type, booktitle, year, pages)

        elif type == types[3]:  # 'thesis'

            try:
                year = int(answer_type['year'])
            except:
                year = 'null'

            thesis_type = str(answer_type['thesis_type'])
            if thesis_type not in ['phd', 'master']:
                thesis_type = 'phd'

            dao_postgres.insert_thesis(id, year, thesis_type)
        else:
            pass



    context = RequestContext(request, {"title": title, "type": type, "id": id})
    return HttpResponse(template.render(context))


@login_required
def get_cited(request):
    template = loader.get_template(
        'related_articles.html')
    answer_type = ""
    if request.method == 'GET':
        answer_type = request.GET
    elif request.method == 'POST':
        answer_type = request.POST

    id = int(answer_type['id'])
    type = "Cited"

    entities = dao_postgres.cited(id)
    context = RequestContext(request, {"type": type, "entities": entities})
    return HttpResponse(template.render(context))

@login_required
def get_cited_by(request):
    template = loader.get_template('related_articles.html')
    answer_type = ""
    if request.method == 'GET':
        answer_type = request.GET
    elif request.method == 'POST':
        answer_type = request.POST
    id = int(answer_type['id'])
    type = "Citedby"
    entities = dao_postgres.cited_by(id)
    context = RequestContext(request, {"type": type, "entities": entities})
    return HttpResponse(template.render(context))

@login_required
def get_related(request):
    template = loader.get_template('related_articles.html')
    answer_type = ""
    if request.method == 'GET':
        answer_type = request.GET
    elif request.method == 'POST':
        answer_type = request.POST
    type = "Authors"
    id = int(answer_type['id'])
    authors = dao_postgres.get_authors_by_entity_id(id)
    entities = []
    for a_id in authors:
        entity = dao_postgres.get_related_entities_by_author(a_id, id)
        entities.append(entity)

    context = RequestContext(request, {"type": type, "id": id, "authors": authors, "entities": entities})
    return HttpResponse(template.render(context))



@login_required
def article(request):
    template = loader.get_template('article.html')
    answer_type = ""
    if request.method == 'GET':
        answer_type = request.GET
    elif request.method == 'POST':
        answer_type = request.POST

    title = str(answer_type['title'])
    type = str(answer_type['type'])
    id = int(answer_type['id'])
    print(id, type, title)

    article = ''
    book = ''
    inclusion = ''
    thesis = ''
    # entity = dao_postgres.get_entity_by_id(val)
    if type in types:
        if type == types[0]:  # 'article'
            article = dao.get_article_by_id(id) #test
            # TODO: Journal?
        elif type == types[1]:  # 'book'
            book = dao_postgres.get_book_by_id(id)
        elif type == types[2]:  # 'inclusion'
            inclusion = dao_postgres.get_inclusion_by_id(id)
        elif type == types[3]:  # 'thesis'
            thesis = dao_postgres.get_thesis_by_id(id)
            # TODO: School?
        else:
            pass

    context = RequestContext(request, {"title": title, "id": id, "type": type, "article": article, "book": book,
                                       "inclusion": inclusion, "thesis": thesis})

    return HttpResponse(template.render(context))

@login_required
def sort_id(request):
    template = loader.get_template('results.html')
    answer_type = ""
    if request.method == 'GET':
        answer_type = request.GET
    elif request.method == 'POST':
        answer_type = request.POST

    title = ""
    title = answer_type['simple-search']
    entities = dao_postgres.get_entity_by_sorted_id(title)  #
    context = RequestContext(request, {"entities": entities, "simple-search": title})
    return HttpResponse(template.render(context))

@login_required
def sort_title(request):
    template = loader.get_template('results.html')
    answer_type = ""
    if request.method == 'GET':
        answer_type = request.GET
    elif request.method == 'POST':
        answer_type = request.POST

    # entities_list = models.record
    title = ""
    title = answer_type['simple-search']
    entities = dao_postgres.get_entity_by_sorted_title(title)  #
    context = RequestContext(request, {"entities": entities, "simple-search": title})
    return HttpResponse(template.render(context))

@login_required
def search_by_id(request):
    template = loader.get_template('search_by_id.html')
    context = RequestContext(request)
    return HttpResponse(template.render(context))