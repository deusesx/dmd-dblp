# -*- coding: utf-8 -*-
# STANDALONE
class __abstract_standalone(object):
    def __init__(self, ID, name):
        self.ID = ID
        self.name = name

    ID = 0
    name = ""

    def __str__(self):
        return self.name


class author(__abstract_standalone):
    pass


class editor(__abstract_standalone):
    pass


class publisher(__abstract_standalone):
    pass


class school(__abstract_standalone):
    pass


class journal(__abstract_standalone):
    def __init__(self, ID, name):
        super(journal, self).__init__()

    # list of articles
    articles = []


# One-To-One

class crossref:
    # record
    ref_record = None


# Many-To-Many

class citation:
    # records
    cite_record = None


# ENTITIES

class record:
    def __init__(self, ID, mdate, title, type):
        self.ID = ID
        self.mdate = mdate
        self.title = title
        self.type = type

    ID = 0
    mdate = ""
    title = ""
    #   article, book, inclusion, thesis, proceedings
    type = None

    def __str__(self):
        return self.title


class __abstract_entity(object):
    def __init__(self, ID, ref_record, year, url="", isbn="", ee='', note={}):
        self.ID = ID
        self.ref_record = ref_record
        self.year = year
        self.url = url
        self.isbn = isbn
        self.ee = ee
        self.note = note

    ref_record = None
    ID = 0
    year = 0
    # author list
    authors = []
    url = ""
    # dict of name and value {value="", type=""}
    # type: urn, unicode, name, dnb, issn, award, isbn, affiliation,undefined
    note = {}
    isbn = ""
    ee = ""

    def __str__(self):
        return self.ref_record.title if self.ref_record else ""


class thesis(__abstract_entity):
    def __init__(self, ID, ref_record, year, type):
        super(thesis, self).__init__(ID, ref_record, year)
        self.type = type

    # school
    ref_school = None
    # master, phd
    type = ""


class inclusion(__abstract_entity):
    def __init__(self, ID, ref_record, year, booktitle, pages):
        super(inclusion, self).__init__(ID, ref_record, year)
        self.booktitle = booktitle
        self.pages = pages

    pages = ""
    booktitle = ""
    # record
    crossref = None


class book(__abstract_entity):
    def __init__(self, ID, ref_record, year, type, volume="", series="", booktitle=""):
        super(book, self).__init__(ID, ref_record, year)
        self.booktitle = booktitle
        self.volume = volume
        self.series = series
        self.type = type

    booktitle = ""
    volume = ""
    series = ""
    type = ""


class article(__abstract_entity):
    def __init__(self, ID, ref_record, year, pages, num, volume):
        super(article, self).__init__(ID, ref_record, year)
        self.pages = pages
        self.num = num
        self.volume = volume

    journal = None
    school = None

# testing
def test_model():
    r = record(ID = 2, key = "key_huhuh", mdate="24.03.2014", type="article", title="Test article")
    print(r)
    a = article(ID = 1, year = 2013, pages = "22-55", num="23", volume=4, ref_record=r)
    print(a)
