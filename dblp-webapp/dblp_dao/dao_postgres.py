import models
import psycopg2

def connect(func):
    def connect_and_cursor(*args, **kwargs):
        res = ''
        try:
            conn_str = "dbname='dblp' user='app_rw' host='dblp.cqpsthbtxkwu.us-west-2.rds.amazonaws.com' port='5432' password='qweasd'"
            conn = psycopg2.connect(conn_str)
            cur = conn.cursor()
            res = func(cur = cur, *args, **kwargs)
            cur.close()
            conn.commit()
            conn.close()
        except:
            raise

        return res

    connect_and_cursor.__name__ = func.__name__
    return connect_and_cursor

@connect
def get_whole_entity_by_id(ID, cur = None):
    pass


@connect
def get_citations(ID, cur = None):
    pass


@connect
def get_entity_by_title(search_title, cur = None):
    entities = []
    eng = "english"
    cur.execute("select entity_id, title, mdate, type FROM dblp_entity WHERE to_tsvector('%s',dblp_entity.title) @@ to_tsquery('%s') LIMIT 10;"  % (eng,search_title))
    for entity in cur.fetchall():
        record = models.record(ID=entity[0], mdate=entity[2], title=entity[1], type=entity[3])
        entities.append(record)
        # print(entity)

    return entities

@connect
def get_entity_by_sorted_id(search_title, cur=None):
    entities = []
    eng = "english"
    cur.execute(
        "select entity_id, title, mdate, type FROM dblp_entity WHERE to_tsvector('%s',dblp_entity.title) @@ to_tsquery('%s') ORDER BY entity_id LIMIT 10;" % (
        eng, search_title))
    for entity in cur.fetchall():
        record = models.record(ID=entity[0], mdate=entity[2], title=entity[1], type=entity[3])
        entities.append(record)
        # print(entity)

    return entities

@connect
def get_entity_by_sorted_title(search_title, cur=None):
    entities = []
    eng = "english"
    cur.execute(
        "select entity_id, title, mdate, type FROM dblp_entity WHERE to_tsvector('%s',dblp_entity.title) @@ to_tsquery('%s') ORDER BY title LIMIT 10;" % (
        eng, search_title))
    for entity in cur.fetchall():
        record = models.record(ID=entity[0], mdate=entity[2], title=entity[1], type=entity[3])
        entities.append(record)
        # print(entity)

    return entities


@connect
def delete_entity_by_id(id, cur=None):
    cur.execute("select sp_del_dblp_entity(%d);" % id)
    # cur.callproc('sp_del_dblp_entity(%d)' % id)
    # cursor.callproc("sp_del_dblp_entity", (id))
    # cursor.callproc("fn_sp_name", (id))


# update title
@connect
def update_entitiy_title(id, title, cur=None):
    # print ("select dblp_update_entity(%d,'%s');" % (id , title))
    cur.execute("select dblp_update_entity(%d,'%s');" % (id, title))
    # cur.callproc('dblp_update_entity', [id, title])
    # print t


@connect
def update_entitiy_article(id, volume, year, num, pages, cur=None):
    cur.execute("select dblp_update_article(%d,'%s', %d, '%s', '%s');" % (id, volume, year, num, pages))


@connect
def update_entitiy_book(id, booktitle, volume, year, cur=None):
    cur.execute("select dblp_update_book(%d,'%s', '%s', %d);" % (id, booktitle, volume, year))


@connect
def update_entitiy_inclusion(id, booktitle, year, pages, cur=None):
    cur.execute("select dblp_update_inclusion(%d,'%s', %d, '%s');" % (id, booktitle, year, pages))


@connect
def update_entitiy_thesis(id, year, cur=None):
    cur.execute("select dblp_update_thesis(%d, %d);" % (id, year))

'''@connectd
def get_entity_by_id(id, cur = None):
    cur.execute("select * from dblp_get_entity(%d);" % id)'''  # artur stuff


@connect
def get_entity_by_id(ID, cur=None):
    entities = []
    cur.execute("select entity_id, mdate, title, type from dblp_entity where entity_id = %d;" % ID)

    for entity in cur.fetchall():
        record = models.record(ID=entity[0], mdate=entity[1], title=entity[2], type=entity[3])
        entities.append(record)
        print(entity)
    return entities

#DONE
@connect
def get_article_by_id(ID, cur=None):
    articles = []
    cur.execute("select entity_id, volume, year, num, pages from dblp_entity_article where entity_id = %d;" % ID)
    for entity in cur.fetchall():
        article = models.article(ID=entity[0], volume=entity[1], year=entity[2], num=entity[3], pages=entity[4],
                                 ref_record=None)
        articles.append(article)
        print(article)
    return articles


@connect
def get_book_by_id(ID, cur=None):
    books = []
    cur.execute(
        "select entity_id, booktitle, volume, year, series_id, type from dblp_entity_book where entity_id = %d;" % ID)
    for entity in cur.fetchall():
        book = models.book(ID=entity[0], booktitle=entity[1], volume=entity[2], year=entity[3], series=entity[4],
                           type=entity[5], ref_record=None)
        books.append(book)
        print(book)
    return books


@connect
def get_inclusion_by_id(ID, cur=None):
    inclusions = []
    cur.execute("select entity_id, booktitle, year, pages from dblp_entity_inclusion where entity_id = %d;" % ID)
    for entity in cur.fetchall():
        inclusion = models.inclusion(ID=entity[0], booktitle=entity[1], year=entity[2], pages=entity[3],
                                     ref_record=None)
        inclusions.append(inclusion)
        print(inclusion)
    return inclusions


@connect
def get_thesis_by_id(ID, cur=None):
    thesis = []
    cur.execute("select entity_id, year, type from dblp_entity_thesis where entity_id = %d;" % ID)
    for entity in cur.fetchall():
        paper = models.thesis(ID=entity[0], year=entity[1], type=entity[2], ref_record=None)
        thesis.append(paper)
        print(paper)
    return thesis

@connect
def insert_entity(p_title, p_type, cur=None):
    cur.execute("SELECT dblp_insert_entity('%s','%s');" % (p_title,p_type))
    i = cur.fetchone()
    id = int(str(i)[1:-3])
    return id

@connect
def insert_article(p_entity_id, p_volume, p_year, p_num, p_pages, cur=None):
    cur.execute("SELECT dblp_insert_article(%d,'%s',%d,'%s','%s');" % (p_entity_id, p_volume, p_year, p_num, p_pages))


@connect
def insert_book(p_entity_id,p_booktitle,p_volume,p_year,p_type, cur=None):
    cur.execute("SELECT dblp_insert_book(%d,'%s','%s',%d,'%s');" % (p_entity_id,p_booktitle,p_volume,p_year,p_type))

@connect
def insert_inclusion(p_entity_id,p_type,p_booktitle,p_year,p_pages, cur=None):
    cur.execute("SELECT dblp_insert_inclusion(%s,'%s','%s',%d,'%s');" % (p_entity_id,p_type,p_booktitle,p_year,p_pages))

@connect
def insert_thesis(p_entity_id,p_year,p_type, cur=None):
    cur.execute("SELECT dblp_insert_thesis(%d,%d,'%s');" % (p_entity_id,p_year,p_type))


@connect
def get_entities_by_author_id(author_id, cur = None):
    pass


@connect
def get_by_schoolname(sch_name, cur = None):
    pass

@connect
def cited(entity_id,cur=None):
    entities = []
    cur.execute("SELECT dblp_get_cited(%d)" % entity_id)
    for entity in cur.fetchall():
        temp = str(entity)[3:-4].split(",")
        record = models.record(ID=int(temp[0]), title=temp[1], type=temp[2], mdate=None)
        entities.append(record)
    return entities

@connect
def cited_by(entity_id,cur=None):
    entities = []
    cur.execute("SELECT dblp_get_citations(%d)" % entity_id)
    for entity in cur.fetchall():
        temp = str(entity)[3:-4].split(",")
        record = models.record(ID=int(temp[0]), title=temp[1], type=temp[2], mdate=None)
        entities.append(record)
    return entities


@connect
def get_authors_by_entity_id(ID, cur=None):
    authors = []
    cur.execute("SELECT author_id FROM dblp_entity_author WHERE entity_id = %d;" % ID)
    for author_id in cur.fetchall():
        author_id = int(str(author_id)[1:-2])  # because returned value is tuple - when we split
        authors.append(author_id)
    return authors


@connect
def get_related_entities_by_author(author_id, ID, cur=None):
    entities = []
    cur.execute("select dblp_related_by_authors(%d,%d);" % (author_id, ID))

    for entity in cur.fetchall():
        # print (entity)
        entity2 = str(entity).split(",")

        # print (entity2)

        # print(entity2[0])
        # print(entity2[1])
        #print(entity2[2])
        entity2[0] = entity2[0][3:len(entity2[0])]
        # print(entity2[0])
        entity2[1] = str(entity2[1][1:-1])
        entity2[2] = str(entity2[2][0:-2])
        # print(entity2[2])
        #print(entity2[1])
        # print (entity2)
        record = models.record(ID=entity2[0], mdate="", title=entity2[1], type=entity2[2])

        entities.append(record)
        # print(entity)
    return entities


#get_related_entities_by_author(110436, 989166)

# get_authors_by_entity_id(989166)

    # TODO: Insert
    # TODO insert_dblp_entity - insert
# TODO User print title, select type, and postgres automatically add mdate and entity_id. key=null automatically
# TODO Second phase of insert: based on type: insert to diff columns
# TODO insert_dblp_article - volume, year, num, pages -should be entered by user. journal_id = null automatically
# TODO insert_dblp_book -booktitle, volume, year, type(selected - proceedings or book) -should be entered by user. series_id = null automatically
# TODO insert_dblp_inclusion - type(selected - inproceedings or incollection), booktitle, year,  pages -should be entered by user. crossref = null automatically
# TODO insert_dblp_thesis -year, type(selected - phd or master) -should be entered by user. school_id = null automatically
