# -*- coding: utf-8 -*-
from Client import *
import json

HOST = "localhost"
PORT = 8080

# query = 'select title, entity-id, mdate from entity where entity-id=1'
# query = "update entity set title='test' where entity-id=1"
# query = "insert into entity(entity-id, mdate, title, type) values (1, '2014-02-05', 'test record', 'article')"
query = "delete from entity where entity-id=1"
# query = "search:mathematics"

remote_answer = ""

client = Client(HOST, PORT)  # initialize our socket
client.send(query)  # send our query
remote_answer = client.recv()  # receive answer from server

print(remote_answer)
ans = json.loads(remote_answer)
print("-----------------------------")
print(ans)
