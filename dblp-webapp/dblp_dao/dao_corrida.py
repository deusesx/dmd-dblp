from dblp_dao.Client import Client
import models
import json
import time



def connect(func):
    def connect_and_cursor(*args, **kwargs):
        res = ''
        try:
            HOST = "localhost"
            PORT = 8080
            cur = Client(HOST, PORT)  # initialize our socket
            res = func(cur = cur, *args, **kwargs)
        except:
            raise

        return res

    connect_and_cursor.__name__ = func.__name__
    return connect_and_cursor

@connect
def get_entity_by_title(search_title, cur = None):
    entities = []
    cur.send("search:" + search_title)
    resp = cur.recv()
    print(resp)
    resp = json.loads(resp)
    if (len(resp)>0):
        for ans in resp:
            entity = models.record(ID=ans['entity-id'], title = ans['title'], mdate=ans['mdate'], type=ans['type'])
            entities.append(entity)
    print(entities)
    return entities

@connect
def delete_entity_by_id(id, cur=None):
    cur.send("delete from entity where entity-id=%s" % id)
    print(cur.recv())

@connect
def update_entitiy_title(id, title, cur=None):
    cur.send("update entity set title='%s' where entity-id=%s" % (id, title))
    print(cur.recv())

@connect
def update_entitiy_article(id, volume, year, num, pages, cur=None):
    cur.send("update article set volume=%s where article-id=%s" % (volume, id))
    cur.recv()
    time.sleep(1)
    HOST = "localhost"
    PORT = 8080
    cur = Client(HOST, PORT)
    cur.send("update article set year=%s where article-id=%s" % (year, id))
    cur.recv()
    time.sleep(1)
    cur = Client(HOST, PORT)
    cur.send("update article set num='%s' where article-id=%s" % (num, id))
    cur.recv()
    time.sleep(1)
    cur = Client(HOST, PORT)
    cur.send("update article set pages='%s' where article-id=%s" % (pages, id))
    print(cur.recv())
    time.sleep(1)

@connect
def get_entity_by_id(ID, cur=None):
    entities = []
    cur.send("select entity-id, mdate, title, type from entity where entity-id = %s" % ID)
    ans = json.loads(cur.recv())

    if (len(ans)>0):
        ans = ans[0]
        entity = models.record(ID=ans['entity-id'], title = ans['title'], mdate=ans['mdate'], type=ans['type'])
        entities.append(entity)
    print(entities)
    time.sleep(1)
    return entities


@connect
def get_article_by_id(ID, cur=None):
    articles = []
    cur.send("select article-id, pages, volume, year, journal-id, num from article where article-id = %s" % ID)
    ans = json.loads(cur.recv())
    if (len(ans)>0):
        ans = ans[0]
        article = models.article(ID=ans['article-id'], volume=ans['volume'], year=ans['year'], num=ans['num'], pages=ans['pages'],                                ref_record=None)
        articles.append(article)
    print(articles)
    return articles

last_id = 1
# returns entity_id
@connect
def insert_entity(p_title, p_type, cur=None):
    id = -1
    global last_id
    cur.send("insert into entity(entity-id, mdate, title, type) values (%d, '%s', '%s', '%s')" % (last_id, '2015-11-23', p_title, p_type))
    print(cur.recv())
    id = last_id
    last_id = last_id + 1
    time.sleep(1)
    return id

@connect
def insert_article(p_entity_id, p_volume, p_year, p_num, p_pages, cur=None):
    cur.send("insert into article(article-id, pages, volume, year, journal-id, num) values "
             "(%s, '%s', %s, %s, %s, '%s')" %
             (p_entity_id, p_pages, p_volume, p_year, 0, p_num))
    res =  cur.recv()
    time.sleep(1)
    print(res)

# ToDo
@connect
def get_entities_by_author_id(author_id, cur = None):
    pass