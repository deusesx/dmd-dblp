import dao_postgres

engine = dao_postgres


# Interface for duck style

def get_entity_by_id(ID):
    return engine.get_entity_by_id(ID)


# defines by type and retrieves associated data
def get_whole_entity_by_id(ID):
    return engine.get_entity_by_id(ID)


def get_citations(ID):
    return engine.get_citations(ID)


def get_entity_by_title(title):
    return engine.get_entity_by_title(title)


def get_entity_by_title_year(title, year):
    return engine.get_entity_by_title_year(title, year)


def get_entities_by_author_name(auth_name):
    return engine.get_entities_by_author_name(auth_name)


def get_by_schoolname(sch_name):
    return engine.get_by_schoolname(sch_name)
