# -*- coding: utf-8 -*-
import socket


class Client:
    def __init__(self, HOST, PORT):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect((HOST, PORT))

    # 1) we split our string to "packages" and send number of "packages"
    # 2) in loop we send our "packages"
    def send(self, string_to_send):
        n = int(len(string_to_send) / 1024) + 1
        self.sock.sendall((str(n) + "\n").encode())
        for i in range(0, n):
            self.sock.sendall((string_to_send[1024 * i:1024 * (i + 1)] + "\n").encode())

    # 1) we receive number of "packages"
    # 2) in loop we get our "packages" and combine to one string
    def recv(self):
        answer = "[]"
        try:
            n = int(self.sock.recv(1024))
            answer = ""
            for i in range(0, n):
                data = bytes.decode(self.sock.recv(1024))
                answer += data.rstrip('\n')
            if answer[0] == '1':
                answer = answer[1:]
        except:
            print("Exception")
        return answer
