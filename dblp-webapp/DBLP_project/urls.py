from django.conf.urls import include, url
from django.http import HttpResponseRedirect
from dblp import urls as dblp_urls
from django.conf import settings
from django.contrib.staticfiles import views
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/profile', lambda x: HttpResponseRedirect('/')),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^', include(dblp_urls))

]

if settings.DEBUG:
    urlpatterns += [
        url(r'^static/(?P<path>.*)$', views.serve),
    ]
